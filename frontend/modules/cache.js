export const cache = {
  get (key) {
    if (process.env.NODE_ENV === 'development') {
      const query = new URLSearchParams(location.search)
      if (query.get('nocache') !== null) {
        return
      }
    }

    return JSON.parse(sessionStorage.getItem(key))
  },
  set (key, value) {
    try {
      sessionStorage.setItem(key, JSON.stringify(value))
    } catch (error) {
      console.error(error)
    }
  },
}
