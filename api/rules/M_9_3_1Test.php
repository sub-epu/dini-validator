<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_9_3_1Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('Identify/invalid-description');
        $this->assertEquals('Identify', $result->issues[0]->verb);
        $this->assertEquals('Schema validation errors in <a>$1</a>: $2', $result->issues[0]->text);
        $this->assertEquals('Identify', $result->issues[0]->values[0]);
        $this->assertCount(2, $result->issues[0]->values);

        $result = runRule('Identify/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
