import { URL, fileURLToPath } from 'node:url'

import { defineConfig } from 'vite'

import componentsAutoImport from 'unplugin-vue-components/vite'
import eslint from 'vite-plugin-eslint'
import rewriteAll from 'vite-plugin-rewrite-all' // Required for routes containing "."
import stylelint from 'vite-plugin-stylelint'
import unfonts from 'unplugin-fonts/vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  css: {
    preprocessorOptions: {
      scss: {
        // This gets prepended to every SCSS file and <style lang="scss"/> block
        additionalData: `
          @use 'sass:color';
          @import '@/styles/settings';
          @import '@/styles/mixins';
        `,
      },
    },
  },
  plugins: [
    componentsAutoImport({
      resolvers: [
        (componentName) => {
          if (componentName.startsWith('Icon')) {
            return {
              from: `vue-material-design-icons/${componentName.replace('Icon', '')}.vue`,
            }
          }

          return {
            from: `@/components/${componentName}.vue`,
          }
        },
      ],
    }),
    eslint({
      cache: true,
      fix: true,
    }),
    rewriteAll(),
    stylelint({
      fix: true,
    }),
    unfonts({
      fontsource: {
        families: [
          {
            name: 'Barlow',
            subset: 'latin',
            weights: [300, 500, 600, 700],
          },
          {
            name: 'Noto Sans Mono',
            subset: 'latin',
            weights: [400],
          },
        ],
        preload: true,
      },
    }),
    vue(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('frontend', import.meta.url)),
    },
  },
})
