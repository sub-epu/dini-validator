<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_10_3Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListRecords/small-repo');
        $this->assertEquals(0, $result->issuesCount);

        $result = runRule('ListRecords/no-responsedate');
        $this->assertEquals(
            '<code>responseDate</code> is missing in <a>ListRecords</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/invalid-responsedate');
        $this->assertEquals(
            '<code>responseDate</code> is invalid in <a>ListRecords</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/no-expirationdate');
        $this->assertEquals(
            '<code>resumptionToken</code> without <code>expirationDate</code> in <a>ListRecords</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/invalid-expirationdate');
        $this->assertEquals(
            '<code>resumptionToken</code> with invalid <code>expirationDate</code> in <a>ListRecords</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/expiring-too-soon');
        $this->assertEquals(
            'The lifespan of the <code>resumptionToken</code> in <a>ListRecords</a> is 3&nbsp;h.',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/fake-100');
        $this->assertEquals(0, $result->issuesCount);
    }
}
