<?php

declare(strict_types=1);

require __DIR__ . '/../bootstrap.php';
require __DIR__ . '/helpers.php';

// Start mock OAI API using the built-in PHP web server
// https://www.php.net/manual/en/features.commandline.webserver.php
exec('php -S localhost:8002 -t api/test/mock-oai-api >/dev/null 2>&1 & echo $!', $output);
$pid = (int) $output[0];

// Kill the local web server when the process ends
register_shutdown_function(function () use ($pid) {
    exec("kill $pid");
});
