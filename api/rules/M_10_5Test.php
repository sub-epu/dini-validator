<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_10_5Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListRecords/small-repo');
        $this->assertEquals(0, $result->issuesCount);

        $result = runRule(
            'ListRecords/good',
            ['secondListRecordsFile' => ''],
        );
        $this->assertEquals(
            'XML file not found',
            getIssueText($result->issues[0]),
        );

        $result = runRule(
            'ListRecords/good',
            ['savedResultFile' => __DIR__ . '/../test/xml/ListRecords/invalid.xml'],
        );
        $this->assertEquals(
            'Invalid response to <a>ListRecords (1)</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule(
            'ListRecords/bad-resumptiontoken',
            ['savedResultFile' => __DIR__ . '/../test/xml/ListRecords/bad-resumptiontoken.xml'],
        );
        $this->assertEquals(
            'Invalid response to <a>ListRecords (2)</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule(
            'ListRecords/unstable-resumptiontoken',
            ['savedResultFile' => __DIR__ . '/../test/xml/ListRecords/unstable-resumptiontoken.xml'],
        );
        $this->assertEquals(
            'Consecutive <a>ListRecords</a> requests do not match.',
            getIssueText($result->issues[0]),
        );

        $result = runRule(
            'ListRecords/good',
            ['savedResultFile' => __DIR__ . '/../test/xml/ListRecords/good-2.xml'],
        );
        $this->assertEquals(0, $result->issuesCount);
    }
}
