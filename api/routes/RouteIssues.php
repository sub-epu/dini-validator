<?php

declare(strict_types=1);

namespace Dini\Validator;

class RouteIssues extends Route
{
    public function serve(): void
    {
        $taskId = $this->sanitizeTaskId($_GET['taskId'] ?? '');
        $ruleId = $this->sanitizeRuleId($_GET['ruleId'] ?? '');
        $n = (int) ($_GET['n'] ?? 1);

        $cacheDir = Config::$cacheDir . "/$taskId";

        if (! $taskId || ! is_dir($cacheDir)) {
            $this->fail('Unknown task ID', 404);
        }

        $issuesFile = "$cacheDir/issues/$ruleId/$n.json";

        if (! file_exists($issuesFile)) {
            $this->fail('Unknown issues file', 400);
        }

        $issuesFile = "$cacheDir/issues/$ruleId/$n.json";
        $content = file_get_contents($issuesFile);

        if ($content === false) {
            $this->fail('Error reading issues', 500);
        }

        echo $content;
    }
}
