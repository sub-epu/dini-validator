<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_11_11Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListMetadataFormats/empty');
        $this->assertEquals(
            'Invalid response to <a>ListMetadataFormats</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListMetadataFormats/no-datacite');
        $this->assertEquals(
            '<code>oai_datacite</code> is missing in <a>ListMetadataFormats</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListMetadataFormats/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
