<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_10_4Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListRecords/small-repo');
        $this->assertEquals(0, $result->issuesCount);

        $result = runRule('ListRecords/no-completelistsize');
        $this->assertEquals(
            '<code>resumptionToken</code> without <code>completeListSize</code> in <a>ListRecords</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/fake-100');
        $this->assertEquals(0, $result->issuesCount);
    }
}
