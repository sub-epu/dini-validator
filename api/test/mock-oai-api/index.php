<?php

declare(strict_types=1);

header('Access-Control-Allow-Headers: authorization, origin, content-type, accept');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Origin: *');

header('Content-Type: application/xml');

$validVerbs = [
    'GetRecord',
    'Identify',
    'ListIdentifiers',
    'ListMetadataFormats',
    'ListRecords',
    'ListSets',
];

$verb = $_GET['verb'] ?? '';
$identifier = preg_replace('#[^a-zA-Z0-9_.-]#', '_', $_GET['identifier'] ?? '');
$metadataPrefix = preg_replace('#[^a-zA-Z0-9_.-]#', '_', $_GET['metadataPrefix'] ?? '');
$resumptionToken = preg_replace('#[^a-zA-Z0-9_.-]#', '_', $_GET['resumptionToken'] ?? '');

if (! in_array($verb, $validVerbs)) {
    echo '<error>Invalid verb</error>';
    exit(400);
}

$xmlDir = __DIR__ . "/../xml/$verb" . ($metadataPrefix === 'oai_datacite' ? '-datacite' : '');

if ($identifier) {
    $file = "$xmlDir/$identifier.xml";

    if (file_exists($file)) {
        echo file_get_contents($file);
    } else {
        http_response_code(404);
    }
} elseif (! $resumptionToken) {
    echo file_get_contents("$xmlDir/good.xml");
} else {
    echo file_get_contents("$xmlDir/good-$resumptionToken.xml");
}
