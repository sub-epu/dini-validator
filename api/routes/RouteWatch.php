<?php

declare(strict_types=1);

namespace Dini\Validator;

class RouteWatch extends Route
{
    protected string $contentType = 'text/event-stream';

    public function serve(): void
    {
        header('Cache-Control: no-store');
        set_time_limit(0);

        $taskId = $this->sanitizeTaskId($_GET['taskId'] ?? '');

        $cacheDir = Config::$cacheDir . "/$taskId";

        if (! $taskId || ! is_dir($cacheDir)) {
            $this->sendEventStreamError(404);

            return;
        }

        $status = $this->readJson("$cacheDir/status.json");

        if (! $status) {
            $this->sendEventStreamError('Status file missing');

            return;
        }

        if ($status->stage === 'failed') {
            $this->sendEventStreamResponse($status);

            return;
        }

        if ($status->progress === 1) {
            $this->sendResult();

            return;
        }

        $shm = shm_attach($status->processId);

        if (! $shm) {
            $this->sendEventStreamError('Error connecting to shared memory');

            return;
        }

        if (! file_exists("/proc/$status->processId") && $status->progress < 1) {
            $this->sendEventStreamError('Validation ended prematurely');

            $status->stage = 'failed';
            $status->progress = 1;

            if (shm_has_var($shm, 0)) {
                shm_remove_var($shm, 0);
            }

            shm_remove($shm);

            file_put_contents("$cacheDir/status.json", json_encode($status, Config::$jsonFlags));

            return;
        }

        $previousStatus = null;
        while (shm_has_var($shm, 0)) {
            // NOTE: When writing and reading at the exact same time,
            // shm_get_var throws a warning and $value is false
            $status = @shm_get_var($shm, 0);

            if ($status && $status !== $previousStatus) {
                $this->sendEventStreamResponse($status);
            }

            if ($status && $status->progress === 1) {
                break;
            }

            $previousStatus = $status;

            usleep(200000); // 200 ms

            if (connection_aborted()) {
                return;
            }
        }

        $status = $this->readJson("$cacheDir/status.json");
        $this->sendEventStreamResponse($status);

        if ($status->stage !== 'failed') {
            $this->sendResult();
        }
    }

    private function sendEventStreamError(int|string $message): void
    {
        $this->sendEventStreamResponse(['error' => $message]);
    }

    private function sendEventStreamResponse(mixed $payload, bool $jsonEncode = true): void
    {
        unset($payload->processId);

        $data = $jsonEncode ? json_encode($payload, Config::$jsonFlags) : $payload;

        ob_start();
        echo "data: $data\n\n";
        ob_end_flush();
        @ob_flush();
        flush();
    }

    private function sendResult(): void
    {
        $resultRoute = new RouteResult();

        ob_start();
        $resultRoute->serve(true);
        $resultJson = ob_get_clean();

        $this->sendEventStreamResponse("{ \"result\": $resultJson }", false);
    }
}
