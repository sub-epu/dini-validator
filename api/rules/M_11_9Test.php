<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_11_9Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListRecords/bad-record');
        $this->assertEquals(
            'Invalid response to <a>ListRecords</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/no-datacite');
        $this->assertEquals('DataCite not available', $result->notice);

        $result = runRule('ListRecords/no-metadata');
        $this->assertEquals(
            '<code>metadata</code> is invalid in <a>record-1</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/datacite-bad-metadata');
        $this->assertEquals(
            '<code>metadata</code> is invalid in <a>bad-metadata (DataCite)</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/datacite-bad-doi');
        $this->assertEquals(
            'DataCite <code>identifier</code> is not a valid DOI in <a>bad-doi (DataCite)</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/datacite-no-match');
        $this->assertEquals(
            '<code>creator</code> does not match <code>dc:creator</code> in <a>record-1 (DataCite)</a>',
            getIssueText($result->issues[0]),
        );
        $this->assertEquals(
            '<code>title</code> does not match <code>dc:title</code> in <a>record-1 (DataCite)</a>',
            getIssueText($result->issues[1]),
        );
        $this->assertEquals(
            '<code>publisher</code> does not match <code>dc:publisher</code> in <a>record-1 (DataCite)</a>',
            getIssueText($result->issues[2]),
        );
        $this->assertEquals(
            '<code>publicationYear</code> does not match <code>dc:issued</code> in <a>record-1 (DataCite)</a>',
            getIssueText($result->issues[3]),
        );
        $this->assertEquals(
            '<code>resourceType</code> does not match <code>dc:type</code> in <a>record-1 (DataCite)</a>',
            getIssueText($result->issues[4]),
        );

        $result = runRule('ListRecords-datacite/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
