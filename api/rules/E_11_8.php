<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_11_8 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    private $coarResourceTypes = [];

    public function setup(): void
    {
        $json = file_get_contents(Config::$dataDir . '/coar-resource-types-3.1.json');
        $this->coarResourceTypes = json_decode($json);

        foreach ($this->coarResourceTypes as &$type) {
            $type = "http://purl.org/coar/resource_type/$type";
        }
    }

    public function checkRecord($record): void
    {
        if (! $record->metadata) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'metadata',
                (string) $record->header->identifier,
            );

            return;
        }

        $oaiDc = $record->metadata->children('oai_dc', true);

        if (! $oaiDc) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'oai_dc',
                (string) $record->header->identifier,
            );
        } else {
            foreach ($oaiDc->children('dc', true)->type as $dcType) {
                if (in_array($dcType, $this->coarResourceTypes)) {
                    return;
                }
            }

            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                'No <code>$1</code> with valid $2 in <a>$3</a>',
                'dc:type',
                'COAR Resource Type',
                (string) $record->header->identifier,
            );
        }
    }
}
