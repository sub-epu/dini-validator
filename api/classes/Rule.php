<?php

declare(strict_types=1);

namespace Dini\Validator;

class Rule
{
    /**
     * Rule is finished and will not run any further checks.
     */
    public bool $finished;

    /**
     * Rule can only be checked manually
     */
    public readonly bool $manual;

    /**
     * OAI verb for this rule
     * Determines on which downloaded XML files the rule is run.
     */
    protected string $oaiVerb = '';

    /**
     * @var object[]
     */
    private array $issues = [];

    private int $issuesCount = 0;

    private string $notice = '';

    /**
     * @var string[]
     */
    private array $overriddenMethods = [];

    private int $runsCount = 0;

    private int $runsPassedCount = 0;

    public function __construct(
        public readonly string $id,
        public readonly int $weight = 0,
        protected ?Validator $validator = null,
    ) {
        $class = new \ReflectionClass(get_class($this));
        $parentClass = $class->getParentClass();

        // Not created from a rule class in api/rules and thus must be checked manually
        $this->manual = ! $parentClass;

        if ($parentClass) {
            // Find all public and protected methods in parent class
            $parentMethods = $parentClass->getMethods(
                \ReflectionMethod::IS_PUBLIC ^ \ReflectionMethod::IS_PROTECTED,
            );

            // Find all parent methods that are re-declared in the child class
            foreach ($parentMethods as $parentMethod) {
                $methodName = $parentMethod->getName();

                // Skip magic methods
                if (str_starts_with($methodName, '__')) {
                    continue;
                }

                $declaringClass = $class->getMethod($methodName)->getDeclaringClass()->getName();

                if ($declaringClass === $class->getName()) {
                    $this->overriddenMethods[] = $methodName;
                }
            }
        }
    }

    /**
     * This method is called once for each rule and can be used to load data
     * which is used in every test, e.g. DublinCore data types.
     *
     * For being overridden in child classes.
     */
    public function setup(): void
    {
    }

    /**
     * Add a fatal issue which resets passed-count
     * and ends evaluation for this rule.
     *
     * $text can contain placeholders ($1, $2, etc.), which
     * get replaced by $values in the same order.
     */
    public function addFatalIssue(string $oaiVerb, string $text, int|string ...$values): void
    {
        $this->addIssue($oaiVerb, $text, ...$values);

        $this->runsPassedCount = 0;

        $this->finish();
    }

    /**
     * Add a new issue.
     *
     * $text can contain placeholders ($1, $2, etc.), which
     * get replaced by $values in the same order.
     *
     * $oaiVerb can just be an OAI verb or contain additional parameters
     * separated by &, like in a URL:
     * - Identify
     * - GetRecord&identifier=12345
     */
    public function addIssue(string $oaiVerb, string $text, mixed ...$values): void
    {
        $issue = (object) [
            'verb' => $oaiVerb ?: $this->oaiVerb,
            'text' => $text,
        ];

        if ($values) {
            $issue->values = $values;
        }

        $this->issues[] = &$issue;
        $this->issuesCount++;
    }

    /**
     * Get the OAI verb of this rule to determine on which files it is applied
     */
    public function getOaiVerb(): string
    {
        return $this->oaiVerb;
    }

    /**
     * The the ratio of passed runs to all runs for score calculation
     */
    public function getPassedRatio(): bool|float
    {
        return $this->runsCount
            ? $this->runsPassedCount / $this->runsCount
            : false;
    }

    /**
     * Get the result for this rule for further
     * processing and display in the frontend.
     */
    public function getResult(bool $withIssues = false): object
    {
        $rule = (object) [
            'id' => $this->id,
            'issuesCount' => $this->issuesCount,
            'manual' => $this->manual,
            'notice' => $this->notice,
            'passedRatio' => $this->getPassedRatio(),
            'points' => $this->getPassedRatio() * $this->weight,
            'runsCount' => $this->runsCount,
            'weight' => $this->weight,
        ];

        if ($withIssues) {
            $rule->issues = $this->issues;
        }

        return $rule;
    }

    /**
     * Run validation for this rule and OAI verb.
     */
    public function run(\SimpleXMLElement $xml, bool $isLastBatch = true): void
    {
        if ($this->oaiVerb === 'ListRecords' &&
            in_array('checkRecord', $this->overriddenMethods)
        ) {
            foreach ($xml->ListRecords->record ?? [] as $record) {
                if (isset($this->finished)) {
                    break;
                }

                if ($record->header && (string) $record->header['status'] === 'deleted') {
                    continue;
                }

                $issuesBefore = $this->issues;

                $this->checkRecord($record);

                $this->runsCount++;

                if (count($this->issues) === count($issuesBefore)) {
                    $this->runsPassedCount++;
                }
            }
        } else {
            $issuesBefore = $this->issues;

            $this->check($xml, $isLastBatch);

            $this->runsCount++;

            if (count($this->issues) === count($issuesBefore)) {
                $this->runsPassedCount++;
            }
        }
    }

    /**
     * Remove and return the first $chunkSize or all issues.
     *
     * @return mixed[]
     */
    public function spliceIssues(bool $all = false, int $chunkSize = 1000): array|bool
    {
        if ($all) {
            $issues = $this->issues;
            $this->issues = [];

            return $issues;
        }
        if (count($this->issues) >= $chunkSize) {
            return array_splice($this->issues, 0, $chunkSize);
        }

        return false;
    }

    /**
     * Check a single record.
     *
     * For being overridden in child classes.
     */
    protected function checkRecord(\SimpleXMLElement $record): void
    {
    }

    /**
     * Check OAI XML
     *
     * For being overridden in child classes.
     */
    protected function check(\SimpleXMLElement $xml, bool $isLastBatch): void
    {
    }

    /**
     * Finish the rule
     *
     * Validation of files for this rule ends, i.e. no further checks are run.
     */
    protected function finish(?string $notice = null): void
    {
        $this->finished = true;
        $this->notice = $notice ?? '';
    }

    /**
     * Convert an array of XML errors to a HTML string.
     *
     * @param  \LibXMLError[]  $xmlErrors
     */
    protected function xmlErrorsToHtml(array $xmlErrors): string
    {
        $errors = [];
        foreach ($xmlErrors as $xmlError) {
            // NOTE: Error 1845 is for unknown child namespaces, which are
            // deliberately ignored. Otherwise, there would occur errors like this:
            //   Element '{http://www.openarchives.org/OAI/2.0/oai-identifier}oai-identifier':
            //   No matching global element declaration available, but demanded by the strict wildcard.
            if ($xmlError->code !== 1845) {
                $errors[] = trim($xmlError->message);
            }
        }

        $html = $errors
            ? '<ul><li>' . implode('</li><li>', $errors) . '</li></ul>'
            : '';

        return $html;
    }
}
