# DINI Validator

The DINI Validator is a web application for checking OAI-PMH APIs as part of the DINI certification process.

Users provide the URL of an OAI-PMH API and their email address, select the number of records to check and start the validation process. The validator then thoroughly checks this API by downloading and validating multiple XML responses, during which the progress is shown. Once completed, the results are displayed and an email is sent, containing a permalink to these results.

The validator consists of a PHP-based backend, doubling as a JSON REST API, and a Vue-based frontend.

## Documentation

1. [Setup](doc/setup.md) &ndash; How to set up the application locally and on a server
1. [API](doc/api.md) &ndash; How the API (backend) works
1. [Rulesets](doc/rulesets.md) &ndash; How to modify the ruleset and add new validation rules
1. [Frontend](doc/frontend.md) &ndash; How the frontend works, including translations
