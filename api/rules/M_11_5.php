<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_11_5 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    private $docTypes = [];

    public function setup(): void
    {
        $json = file_get_contents(Config::$dataDir . '/doc-types.json');
        $this->docTypes = json_decode($json);
    }

    public function checkRecord($record): void
    {
        if (! $record->metadata) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'metadata',
                (string) $record->header->identifier,
            );

            return;
        }

        $oaiDc = $record->metadata->children('oai_dc', true);

        if (! $oaiDc) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'oai_dc',
                (string) $record->header->identifier,
            );

            return;
        }

        foreach ($oaiDc->children('dc', true)->type as $dcType) {
            if (in_array($dcType, $this->docTypes)) {
                return;
            }
        }

        $this->addIssue(
            "GetRecord&identifier={$record->header->identifier}",
            'No <code>$1</code> with valid $2 in <a>$3</a>',
            'dc:type',
            'doc-type',
            (string) $record->header->identifier,
        );
    }
}
