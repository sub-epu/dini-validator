<?php

declare(strict_types=1);

namespace Dini\Validator;

use DOMDocument;

class M_9_3_4 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    // Made overridable for unit tests
    public $unknownRecordId = 'unknown-record-id';

    public function check($xml, $isLastBatch): void
    {
        $result = $this->validator->downloadOaiXml('GetRecord', '', $this->unknownRecordId);

        if (! $result || ! $result->content) {
            $this->addFatalIssue(
                "GetRecord&identifier=$this->unknownRecordId",
                'Invalid response to <a>$1</a>',
                'GetRecord',
            );

            return;
        }

        $dom = new DOMDocument();
        $domCreated = @$dom->loadXML($result->content);

        // @codeCoverageIgnoreStart
        if (! $domCreated) {
            $this->addFatalIssue(
                "GetRecord&identifier=$this->unknownRecordId",
                'Invalid response to <a>$1</a>',
                'GetRecord',
            );

            return;
        }
        // @codeCoverageIgnoreEnd

        $dom->schemaValidate(Config::$dataDir . '/schemas/OAI-PMH.xsd');
        $xmlErrors = libxml_get_errors();
        libxml_clear_errors();
        $errorHtml = $this->xmlErrorsToHtml($xmlErrors);

        if ($errorHtml) {
            $this->addIssue(
                "GetRecord&identifier=$this->unknownRecordId",
                'Schema validation errors in <a>$1</a>:<br>$2',
                'GetRecord',
                $errorHtml,
            );
        }

        $this->finish();
    }
}
