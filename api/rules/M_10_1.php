<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_10_1 extends Rule
{
    public string $oaiVerb = 'ListSets';

    public function check($xml, $isLastBatch): void
    {
        foreach ($xml->ListSets->set as $set) {
            if ((string) $set->setSpec === 'open_access') {
                $this->finish();

                return;
            }
        }

        if ($isLastBatch) {
            $this->addFatalIssue(
                'ListSets',
                '<code>$1</code> is missing in <a>$2</a>',
                'open_access',
                'ListSets',
            );

            return;
        }
    }
}
