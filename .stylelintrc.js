module.exports = {
  "extends": [
    "stylelint-stylistic/config",
    "stylelint-config-standard-scss",
    "stylelint-config-standard-vue/scss",
  ],
  "plugins": [
    "stylelint-order",
  ],
  "rules": {
    "declaration-empty-line-before": "never",
    "order/properties-alphabetical-order": true,
    "rule-empty-line-before": [
      "always",
      {
        "ignore": ["after-comment", "first-nested"],
        "severity": "warning",
      }
    ],
    "stylistic/number-leading-zero": "never",
    "stylistic/string-quotes": "single",
  },
}
