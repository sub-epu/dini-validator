<?php

declare(strict_types=1);

namespace Dini\Validator;

class RouteXml extends Route
{
    public function serve(): void
    {
        $taskId = $this->sanitizeTaskId($_GET['taskId'] ?? '');
        $url = @base64_decode($_GET['url'] ?? '');

        $cacheDir = Config::$cacheDir . "/$taskId";

        if (! $taskId || ! is_dir($cacheDir)) {
            $this->fail('Invalid request', 400);
        }

        if (! $url || ! filter_var($url, FILTER_VALIDATE_URL)) {
            $this->fail('Invalid request', 400);
        }

        // Only accept requests if we got a finished validation for this OAI URL
        $status = $this->readJson("$cacheDir/status.json");

        if (! $status
            || $status->stage !== 'finished'
            || ! str_starts_with($url, $status->oaiApiUrl)
        ) {
            $this->fail('Invalid request', 400);
        }

        $streamContext = stream_context_create([
            'http' => [
                'header' => 'User-Agent: ' . Config::$userAgent,
                'timeout' => Config::$oaiApiTimeout,
            ],
        ]);

        $xml = @file_get_contents($url, false, $streamContext);

        if (! $xml) {
            $this->fail('Error loading remote XML', 502);
        }

        // Prettify XML
        $dom = new \DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;

        $domCreated = @$dom->loadXML($xml);

        if ($domCreated) {
            $prettyXml = $dom->saveXML();
            $resumptionToken = $dom->getElementsByTagName('resumptionToken')->item(0)?->textContent;
        } else {
            // This is probably not actual XML, still return what we got from the server
            $prettyXml = $xml;
        }

        $this->sendJson([
            'xml' => $prettyXml,
            'resumptionToken' => $resumptionToken ?? '',
        ]);
    }
}
