<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_11_5 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    public function checkRecord($record): void
    {
        if (! $record->metadata) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'metadata',
                (string) $record->header->identifier,
            );

            return;
        }

        $oaiDc = $record->metadata->children('oai_dc', true);

        if (! $oaiDc) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'oai_dc',
                (string) $record->header->identifier,
            );

            return;
        }

        $dates = $oaiDc->children('dc', true)->date;

        if (! $dates) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'dc:date',
                (string) $record->header->identifier,
            );

            return;
        }

        if (count($dates) > 1) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is used multiple times in <a>$2</a>',
                'dc:date',
                (string) $record->header->identifier,
            );

            return;
        }
    }
}
