<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_9_3_2 extends Rule
{
    public string $oaiVerb = 'ListIdentifiers';

    public function check($xml, $isLastBatch): void
    {
        libxml_use_internal_errors(true);
        $dom = dom_import_simplexml($xml)->ownerDocument;

        // @codeCoverageIgnoreStart
        if (! $dom) {
            $this->addFatalIssue('Invalid XML for <a>$1</a>', 'ListIdentifiers');

            return;
        }
        // @codeCoverageIgnoreEnd

        $dom->schemaValidate(Config::$dataDir . '/schemas/OAI-PMH.xsd');
        $xmlErrors = libxml_get_errors();
        libxml_clear_errors();
        $errorHtml = $this->xmlErrorsToHtml($xmlErrors);

        if ($errorHtml) {
            $this->addIssue(
                'ListIdentifiers',
                'Schema validation errors in <a>$1</a>:<br>$2',
                'ListIdentifiers',
                $errorHtml,
            );
        }

        $this->finish();
    }
}
