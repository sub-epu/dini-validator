<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_11_11 extends Rule
{
    public string $oaiVerb = 'ListMetadataFormats';

    public function check($xml, $isLastBatch): void
    {
        if (empty($xml->ListMetadataFormats->metadataFormat)) {
            $this->addFatalIssue(
                'ListMetadataFormats',
                'Invalid response to <a>$1</a>',
                'ListMetadataFormats',
            );

            return;
        }

        foreach ($xml->ListMetadataFormats->metadataFormat as $metadataFormat) {
            if ((string) $metadataFormat->metadataPrefix === 'oai_datacite') {
                $this->finish();

                return;
            }
        }

        if ($isLastBatch) {
            $this->addFatalIssue(
                'ListMetadataFormats',
                '<code>$1</code> is missing in <a>$2</a>',
                'oai_datacite',
                'ListMetadataFormats',
            );

            return;
        }
    }
}
