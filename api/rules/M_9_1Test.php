<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_9_1Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('Identify/bad-protocol-version');
        $this->assertEquals(
            (object) [
                'verb' => 'Identify',
                'text' => '<a>Identify</a> is invalid',
            ],
            $result->issues[0],
        );

        $result = runRule('Identify/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
