<?php

declare(strict_types=1);

namespace Dini\Validator;

// #[\PHPUnit\Framework\Attributes\CoversClass(M_9_6::class)]
class M_9_6Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('Identify/no-admin-email');
        $this->assertEquals(1, $result->issuesCount);
        $this->assertEquals(
            '<code>adminEmail</code> in <a>Identify</a> does not contain a valid email address.',
            $result->issues[0]->text,
        );

        $result = runRule('Identify/invalid-admin-email');
        $this->assertEquals(1, $result->issuesCount);
        $this->assertEquals(
            '<code>adminEmail</code> in <a>Identify</a> does not contain a valid email address.',
            $result->issues[0]->text,
        );

        $result = runRule('Identify/no-description');
        $this->assertEquals(1, $result->issuesCount);
        $this->assertEquals(
            '<code>$1</code> is missing in <a>$2</a>',
            $result->issues[0]->text,
        );
        $this->assertEquals('description', $result->issues[0]->values[0]);
        $this->assertEquals('Identify', $result->issues[0]->values[1]);

        $result = runRule('Identify/no-english-description');
        $this->assertEquals(1, $result->issuesCount);
        $this->assertEquals(
            '<a>Identify</a> does not seem to have a <code>description</code> in English.',
            $result->issues[0]->text,
        );

        $result = runRule('Identify/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
