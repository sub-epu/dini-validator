<?php

declare(strict_types=1);

namespace Dini\Validator;

require __DIR__ . '/config/Config.php';

error_reporting(E_ALL);

if (! empty(Config::$errorDisplay)) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
} else {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
}

if (! empty(Config::$errorLog)) {
    ini_set('error_log', Config::$errorLog);
    ini_set('log_errors', 1);
}

date_default_timezone_set(Config::$timezone);

require __DIR__ . '/../vendor/autoload.php';

spl_autoload_register(function (string $className): void {
    $shortName = str_replace('Dini\Validator\\', '', $className);

    if (file_exists(__DIR__ . "/rules/{$shortName}.php")) {
        require_once __DIR__ . "/rules/{$shortName}.php";
    } elseif (file_exists(__DIR__ . "/routes/{$shortName}.php")) {
        require_once __DIR__ . "/routes/{$shortName}.php";
    } elseif (file_exists(__DIR__ . "/classes/{$shortName}.php")) {
        require_once __DIR__ . "/classes/{$shortName}.php";
    }
});
