<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_11_9 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    public function checkRecord($record): void
    {
        if (! $record->metadata) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'metadata',
                (string) $record->header->identifier,
            );

            return;
        }

        $oaiDc = $record->metadata->children('oai_dc', true);

        if (! $oaiDc) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'oai_dc',
                (string) $record->header->identifier,
            );

            return;
        }

        $creators = $oaiDc->children('dc', true)->creator;

        if (! $creators) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'dc:creator',
                (string) $record->header->identifier,
            );

            return;
        }

        $creatorsWithOrchidId = array_filter(
            (array) $creators,
            fn ($creator) => preg_match('#https://orcid.org/\d{4}-\d{4}-\d{4}-\d{3}[0-9X]#', (string) $creator),
        );

        if (! $creatorsWithOrchidId) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                'No <code>$1</code> with valid $2 in <a>$3</a>',
                'dc:creator',
                'ORCID iD',
                (string) $record->header->identifier,
            );
        }
    }
}
