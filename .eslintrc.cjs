/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-recommended',
    'plugin:vue-pug/vue3-recommended',
    'plugin:vuejs-accessibility/recommended',
    'plugin:unicorn/recommended',
    '@vue/eslint-config-standard',
  ],
  globals: {
    process: true,
  },
  ignorePatterns: ['cache/*', 'dist/*', 'vendor/*'],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  rules: {
    'comma-dangle': ['error', 'always-multiline'],
    'import/no-extraneous-dependencies': ['error'],
    'import/order': ['error'],
    'no-console': ['warn', { allow: ['warn', 'error'] }],
    'unicorn/filename-case': 0,
    'unicorn/prevent-abbreviations': [
      'error',
      {
        allowList: {
          props: true,
        },
      },
    ],
    'vue/attribute-hyphenation': ['warn', 'never'],
    'vue/v-on-event-hyphenation': ['warn', 'never'],
    'vue/max-attributes-per-line': 0, // This rule damages Pug templates, do not enable!
    'vue/no-template-target-blank': ['error'],
    'vue/no-v-html': 0,
    'vue/require-default-prop': 0,
    'vuejs-accessibility/label-has-for': [
      'error',
      {
        required: {
          some: ['nesting', 'id'],
        },
      },
    ],
    'vue-pug/no-pug-control-flow': 0, // This rule forbids “=' '”, which we need
  },
}
