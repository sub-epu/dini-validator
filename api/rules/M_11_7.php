<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_11_7 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    private $languageCodes = [];

    public function setup(): void
    {
        $json = file_get_contents(Config::$dataDir . '/iso-639-3.json');
        $this->languageCodes = json_decode($json);
    }

    public function checkRecord($record): void
    {
        if (! $record->metadata) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'metadata',
                (string) $record->header->identifier,
            );

            return;
        }

        $oaiDc = $record->metadata->children('oai_dc', true);

        if (! $oaiDc) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'oai_dc',
                (string) $record->header->identifier,
            );

            return;
        }

        $languageCode = (string) $oaiDc->children('dc', true)->language;

        if (! $languageCode) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'dc:language',
                (string) $record->header->identifier,
            );

            return;
        }

        if (! in_array($languageCode, $this->languageCodes)) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is invalid in <a>$2</a>',
                'dc:language',
                (string) $record->header->identifier,
            );
        }
    }
}
