<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_10_4Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('Identify/no-deletedrecord');
        $this->assertEquals(
            '<code>deletedRecord</code> is missing in <a>Identify</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('Identify/invalid-deletedrecord');
        $this->assertEquals(
            '<code>deletedRecord</code> is invalid in <a>Identify</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('Identify/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
