import { ref } from 'vue'

import { fetchJson } from '../modules/http'

import config from '../config'

// NOTE: A single translation is small enough to be imported. Also, we expect
// most users to prefer German. Should there be more translations in the future,
// they should be placed in public/translations and loaded on demand.
import de from '../translations/de.json'

const translationsCache = {
  de,
}

export default {
  install: (app) => {
    const globals = app.config.globalProperties
    const translation = ref(undefined)

    globals.$i18n = {
      defaultLang: config.defaultLang || 'en',
      languages: config.languages || { en: 'English' },
      formatNumber (number, decimals) {
        const locale = globals.$router?.currentRoute.value.params.lang || config.defaultLang
        return (number || '?').toLocaleString(locale, {
          minimumFractionDigits: decimals || 0,
          maximumFractionDigits: decimals || 9,
        })
      },
      async setLanguage (lang) {
        if (!lang || lang === config.defaultLang) {
          translation.value = undefined
        } else if (config.languages[lang]) {
          translation.value = translationsCache[lang] ||
            (await fetchJson(`/translations/${lang}.json`, { cache: 'no-cache' }))
        } else {
          const href = location.href
          globals.$router?.replace({ name: '404' })
          setTimeout(() => history.replaceState(history.state, '', href))
        }

        document.documentElement.lang = lang || config.defaultLang
      },
      translate (string, ...values) {
        string = string.toString()
        let translatedString = string

        if (translation.value && translation.value[string]) {
          translatedString = translation.value[string]
        } else if (translation.value && import.meta.env.DEV) {
          console.warn(`Missing translation for "${string}"`)
        }

        // Replace $1, $2, ...
        return translatedString.replaceAll(
          /\$(\d+)/g,
          (match, index) => values[index - 1] || '?',
        )
      },
    }

    // Shorthand for use in templates
    globals.$t = globals.$i18n.translate

    // Update translation on route change
    globals.$router?.beforeEach(async (route) => {
      globals.$i18n.setLanguage(route.params.lang)
    })
  },
}
