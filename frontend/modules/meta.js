export function setTitle (separator = ' · ', sourceElement = 'h1') {
  const pageTitle = document.querySelector(sourceElement)?.textContent
  const appTitle = document.title.split(separator).at(-1)
  document.title = (!pageTitle || pageTitle === appTitle ? '' : pageTitle + separator) +
    appTitle
}
