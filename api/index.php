<?php

declare(strict_types=1);

require 'bootstrap.php';

$path = ltrim($_SERVER['PATH_INFO'] ?? '', '/');
$classname = 'Dini\Validator\Route' . ucfirst($path);
if ($path && class_exists($classname)) {
    $route = new $classname();
    $route->setHttpHeader();
    $route->serve();
} else {
    http_response_code(404);
    echo json_encode(['error' => 'Unknown route']);
}
