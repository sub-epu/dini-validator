# Rulesets

Rulesets define rules for validating an OAI-PMH API.

## Defining a Ruleset

For every available ruleset version, there must a JSON file in [api/data/rulesets](../api/data/rulesets), e.g. [api/data/rulesets/2022.json](../api/data/rulesets/2022.json) for the ruleset 2022. The JSON consists of an array of rule objects, each with these properties:

- `id` (string, required): A unique identifier for each rule. May only contain characters `A-Z`, `0-9` and `_`, e.g. `M_9_1`.
- `key` (object, required): The `id` for display in the frontend in each supported language. Keys are language codes, values are the translations, e.g. `{ "en": "M.9-1" }`. May contain any characters.
- `title` (object, required): The rule title in each supported language. Keys are language codes, values are the translations, e.g. `{ "en": "OAI-PMH Specification" }`. HTML is not allowed.
- `description` (object, required): The rule description in each supported language. Keys are language codes, values are the translations, e.g. `{ "en": "<p>The OAI API conforms to the protocol specification.</p>" }`. HTML is allowed and encouraged.
- `weight` (number, required): A non-negative integer, denoting the rule’s weight when calculating the final score, e.g. `4`.
- `manual` (boolean, optional): If set to `true`, the rule can only be checked manually. If omitted or `false`, the rule is validated programmatically and requires a PHP rule file in `api/rules` whose name matches the `id`, e.g. `M_9_1.php`.
- `validation` (string, optional): Explanation about how the rule should be validated (not publicly displayed).
- `example` (string, optional): A valid example, usually an XML string (not publicly displayed).
- `note` (string, optional): Internal notes about the rule (not publicly displayed).

The ruleset version is stored with each result. Therefore, **rulesets must never be deleted or changed**, except for small text modifications like updating a rule’s `description`. For larger changes, add a new ruleset with a new version.

Only one ruleset can be active at a time, and the PHP files in `rules` have to match the current ruleset.

Example:

```json
[
  {
    "id": "M_9_1",
    "key": "M.9-1",
    "title": {
      "en": "OAI-PMH Specification",
      "de": "OAI-PMH-Spezifikation"
    },
    "description": {
      "en": "<p>The OAI API conforms to version 2.0 of the protocol specification.</p>",
      "de": "<p>Die OAI-Schnittstelle verhält sich konform gemäß der Protokollspezifikation in der Version 2.0.</p>"
    },
    "example": "<Identify>\n  <protocolVersion>2.0<\/protocolVersion>",
    "manual": false,
    "weight": 4,
    "validation": "verb=Identify, field=protocolVersion\ncontent=2.0"
  },
  …
]
```

## Validation Rules

For each rule of the ruleset which does not have set `manual` to `true`, a rule class has to be provided in `api/rules`, its name matching the rule’s `id` and extendind the `Rule` base class.

### Properties

-
    ```php
    protected string $oaiVerb
    ```

    Sets the OAI verb to which this rule applies; must be one of these strings:

    - `GetRecord`
    - `Identify`
    - `ListIdentifiers`
    - `ListMetadataFormats`
    - `ListRecords`
    - `ListSets`

    The rule is only run on files for this OAI verb.

### Methods

Each rule must implement one validation method, either `check` or `checkRecords`, and may optionally implement the `setup` method.

-
   ```php
    public Rule::setup()
    ```

    No parameters.

    This method is called once for every rule and can be used for expensive tasks that do not have to be executed for every validated file, e.g. loading a common data set.

-
    ```php
    public Rule::addIssue(
        string $oaiVerb,
        string $text,
        int|string ...$values
    ): void
    ```

    #### Parameters

    - **$oaiVerb**
        The OAI verb for which the rule failed, e.g. `ListRecords`.

    - **$text**
        The text for the fatal issue. May contain placeholders like `$1`, `$2`, etc.

    - **$values**
        Replacements for the placeholders in `$text`. The first value replaces `$1`, the second one `$2`, etc.

    This method is only used in `check` or `checkRecord` for adding issues, i.e. things that went wrong during the validation.

-
    ```php
    public Rule::addFatalIssue(
        string $oaiVerb,
        string $text,
        int|string ...$values
    ): void
    ```

    This method works like `addIssue`, except that previously passed runs are reset and rule execution is halted, i.e. the rule is not run on any further files or records.

-
    ```php
    protected function check(
        SimpleXMLElement $xml,
        bool $isLastBatch
    ): void
    ```

    #### Parameters

    - **$xml**
        The XML (as SimpleXMLElement) of the current reply for the OAI verb set via `$oaiVerb`. The rule is run for each downloaded batch.

    - **$isLastBatch**
        Used so the rule knows when it is run for the last time for OAI verbs with multiple batches.

-
    ```php
    protected Rule::checkRecord(SimpleXMLElement $record): void
    ```

    #### Parameters

    - **$record**
        The XML (as SimpleXMLElement) of the current record.

    Each record from the list of records is fed to the rule for validation. Can only be used when `$oaiVerb` is `ListRecords`,

### Issues

Each evocation of the methods `addIssue` or `addFatalIssue` adds another issue for this rule. An issue consists of an OAI `verb`, `text` and optionally `values`:

```json
{
    "verb": "GetRecord&identifier=record-1",
    "text": "No <code>$1</code> with valid $2 in <a>$3</a>",
    "values": [
        "dc:type",
        "doc-type",
        "record-1"
    ]
}
```

`text` may contain HTML and should contain `<a>…</a>`, which is “hydrated” in the frontend into a working link; its `href` is determined by the OAI API base URL, combined with the OAI `verb`, glued by `?verb=`. Placeholders like `$1`, `$2`, etc. are replaced by `values` in the same order.

In the Frontend, this issues would be displayed like this for the OAI API URL `https://example.org/oai`:

```html
No <code>dc:type</code> with valid doc-type in <a href="https://example.org/oai?verb=GetRecord&identifier=record-1">record-1</a>
