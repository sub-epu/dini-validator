<?php

declare(strict_types=1);
declare(ticks=1);

namespace Dini\Validator;

class Validator
{
    /**
     * Cache directory path
     */
    public readonly string $cacheDir;

    /**
     * Maximum retries when downloading remote XML files
     * A short wait time is added before each retry.
     */
    protected int $maxRetries = 2;

    /**
     * OAI verbs to check
     *
     * @var string[]
     */
    protected array $oaiVerbs = [
        'Identify',
        'ListIdentifiers',
        'ListMetadataFormats',
        'ListRecords',
        'ListSets',
    ];

    /**
     * Validation status
     * Gets initiated in __construct().
     */
    protected ?object $status = null;

    private ?\SysvSharedMemory $shm = null;

    private float $startTimestamp = 0;

    public function __construct(string $oaiApiUrl, int $recordsLimit = 0)
    {
        register_shutdown_function([&$this, 'cleanUp']);

        if (function_exists('pcntl_signal')) {
            pcntl_signal(SIGINT, [&$this, 'cleanUp']);
        }

        $this->startTimestamp = microtime(true);

        $domain = strtolower(parse_url($oaiApiUrl, PHP_URL_HOST) ?: '');
        $timestamp = date('Y-m-d\TH-i-s', time());
        $randomString = substr(md5((string) rand()), 0, 8); // collision probability: ~ 1 / 1.8e12
        $taskId = "{$this->urlToDirname($domain)}-$timestamp-$randomString";
        $this->cacheDir = Config::$cacheDir . "/$taskId";

        $this->status = (object) [
            'errors' => [],
            'filesDownloaded' => 0,
            'filesTotal' => 0,
            'oaiApiUrl' => $oaiApiUrl,
            'processId' => getmypid(),
            'progress' => 0,
            'recordsBatchSize' => null,
            'recordsChecked' => 0,
            'recordsDownloaded' => 0,
            'recordsLimit' => $recordsLimit,
            'secondsRemaining' => 0,
            'stage' => 'init',
            'taskId' => $taskId,
        ];

        $this->saveStatus();
    }

    /**
     * Download all XML files from an OAI API which are required for validation.
     */
    public function download(): void
    {
        $status = &$this->status;

        $status->filesDownloaded = 0;
        $status->filesTotal = 0;
        $status->progress = 0;
        $status->recordsDownloaded = 0;

        foreach ($this->oaiVerbs as $oaiVerb) {
            if ($oaiVerb === 'ListRecords') {
                // We get to that below
                continue;
            }

            $resumptionToken = '';
            $status->stage = "downloading $oaiVerb";

            do {
                $result = $this->cacheOaiDcXml($oaiVerb, $resumptionToken);

                if (! $result) {
                    if ($oaiVerb === 'Identify') {
                        $this->status->progress = 1;
                        $this->status->stage = 'failed';
                        $this->saveStatus(true);

                        return;
                    }

                    $this->addError(
                        'XML download failed for $1 ($2)',
                        $oaiVerb,
                        $this->getOaiLink($oaiVerb, $resumptionToken),
                    );
                    break;
                }

                $status->filesDownloaded++;
                $status->filesTotal++;

                if (! empty($resumptionToken) && $result->resumptionToken === $resumptionToken) {
                    $this->addError(
                        'Recursive resumption token for $1, XML download aborted ($2)',
                        $oaiVerb,
                        $this->getOaiLink($oaiVerb, $resumptionToken),
                    );
                    break;
                }

                if ($oaiVerb === 'ListIdentifiers') {
                    // We only need the first batch for ListIdentifiers
                    break;
                }

                $resumptionToken = $result->resumptionToken ?? '';
            } while ($resumptionToken);

            $status->progress += .01;
            $this->saveStatus(false);
        }

        $durations = [];
        $initialProgress = $status->progress;
        $resumptionToken = '';
        $status->stage = 'downloading ListRecords';

        do {
            $result = $this->cacheOaiDcXml('ListRecords', $resumptionToken);

            if (! $result) {
                $this->addError(
                    'XML download aborted for $1 ($2)',
                    'ListRecords',
                    $this->getOaiLink('ListRecords', $resumptionToken),
                );
                break;
            }

            if (empty($resumptionToken) && $result->recordsBatchSize) {
                $status->recordsBatchSize = $result->recordsBatchSize;

                if ($status->recordsBatchSize && $status->recordsLimit) {
                    $status->filesTotal += max(2, ceil($status->recordsLimit / $status->recordsBatchSize));
                } elseif ($result->completeListSize) {
                    $status->filesTotal += ceil($result->completeListSize / $status->recordsBatchSize);
                } else {
                    // There is no way of knowing how many files are going to be downloaded
                    $status->filesTotal = false;
                }
            }

            $status->filesDownloaded++;
            $status->recordsDownloaded += $result->recordsCount;

            if ($status->filesTotal) {
                // Update filesTotal in case completeListSize is wrong, which does happen
                if ($status->filesDownloaded > $status->filesTotal) {
                    $status->filesTotal = $status->filesDownloaded;
                }

                // Last 3% are for evaluation
                $status->progress = round(
                    $initialProgress + $status->filesDownloaded / $status->filesTotal * (.97 - $initialProgress),
                    3,
                );

                // Calculate remaining time from average of last n durations
                $durations[] = $result->duration;
                array_splice($durations, 0, -10);
                $durationsCount = count($durations);

                if ($durationsCount >= 3) {
                    $status->secondsRemaining = ceil(
                        array_sum($durations) / $durationsCount * ($status->filesTotal - $status->filesDownloaded)
                            + ($status->filesTotal * Config::$evaluationDurationPerBatch),
                    );
                }
            } else {
                $status->progress = false;
            }

            $this->saveStatus(false);

            if (! empty($resumptionToken)) {
                if ($status->recordsLimit && $status->recordsDownloaded >= $status->recordsLimit) {
                    break;
                }

                if ($result->resumptionToken === $resumptionToken) {
                    $this->addError(
                        'Recursive resumption token for $1, XML download aborted ($2)',
                        'ListRecords',
                        $this->getOaiLink('ListRecords', $resumptionToken),
                    );
                    break;
                }
            }

            if (time() > $this->startTimestamp + Config::$downloadTimeLimit) {
                $this->addError('Downloading all records took too long, evaluation is incomplete');
                break;
            }

            $resumptionToken = $result->resumptionToken ?? '';
        } while ($resumptionToken);

        $status->stage = 'download complete';
        $status->progress = .97;
        $this->saveStatus();
    }

    /**
     * Download a single XML file from an OAI API.
     */
    public function downloadOaiXml(
        string $oaiVerb,
        string $resumptionToken = '',
        string $identifier = '',
        string $metadataPrefix = 'oai_dc',
    ): bool|object {
        $url = "{$this->status->oaiApiUrl}?verb=$oaiVerb";

        if ($resumptionToken) {
            $url .= "&resumptionToken=$resumptionToken";
        } elseif ($identifier) {
            $url .= "&identifier=$identifier";
        }

        if (! $resumptionToken && in_array($oaiVerb, ['GetRecord', 'ListIdentifiers', 'ListRecords'])) {
            $url .= "&metadataPrefix=$metadataPrefix";
        }

        $streamContext = stream_context_create([
            'http' => [
                'header' => 'User-Agent: ' . Config::$userAgent,
                'timeout' => Config::$oaiApiTimeout,
            ],
        ]);

        $content = false;
        $tries = 0;
        do {
            sleep($tries * 2); // add increasing wait time before each retry
            $content = @file_get_contents($url, false, $streamContext);
            $tries++;
        } while ($resumptionToken && ! $content && $tries <= $this->maxRetries && ! defined('TESTING'));

        if (! $content) {
            return false;
        }

        $xml = @simplexml_load_string($content);

        if (! $xml) {
            $this->addError(
                'Received malformed XML ($1)',
                "<a href='$url'>$url</a>",
            );

            return false;
        }

        return (object) [
            'content' => $content,
            'xml' => $xml,
        ];
    }

    /**
     * Get the validation status object.
     */
    public function getStatus(): ?object
    {
        return $this->status;
    }

    /**
     * Validate all downloaded XML files, i.e. run applicable rules on each file.
     */
    public function validate(): void
    {
        $status = &$this->status;

        // Reset filesTotal to the actually downloaded number instead of relying on completeListSize
        $status->filesTotal = $this->getCachedXmlFilesCount();
        $status->progress = .97;

        if (! $status->recordsBatchSize) {
            $xml = @simplexml_load_file("$this->cacheDir/xml/ListRecords/1.xml");

            if ($xml) {
                $status->recordsBatchSize = count($xml->ListRecords?->record);
            }
        }

        $this->saveStatus();

        $rules = $this->getRules();

        $filesChecked = 0;

        foreach ($this->oaiVerbs as $oaiVerb) {
            $status->stage = "evaluating $oaiVerb";
            $this->saveStatus(false);

            $filenames = glob("{$this->cacheDir}/xml/$oaiVerb/*.xml", GLOB_NOSORT);

            if (! $filenames) {
                $this->addError('No XML files to check for $1', $oaiVerb);

                continue;
            }

            natsort($filenames);

            $applicableRules = array_filter($rules, fn ($rule) => $rule->getOaiVerb() === $oaiVerb);
            $lastIndex = count($filenames) - 1;

            foreach ($filenames as $index => $filename) {
                $this->validateFile($oaiVerb, $applicableRules, $filename, $index === $lastIndex);

                $filesChecked++;

                if ($status->filesTotal) {
                    $filesRemaining = max(0, $status->filesTotal - $filesChecked);
                    $status->secondsRemaining = ceil($filesRemaining * Config::$evaluationDurationPerBatch);

                    // NOTE: First 97% are for downloading
                    $status->progress = round(.97 + $filesChecked / $status->filesTotal * .03, 3);
                }

                $this->saveStatus(false);

                if ($oaiVerb === 'ListRecords'
                    && $status->recordsLimit
                    && $status->recordsChecked >= $status->recordsLimit
                ) {
                    break;
                }
            }
        }

        foreach ($rules as $rule) {
            if (! $rule->manual && $rule->getPassedRatio() === false) {
                $rule->addIssue('', 'Not checked due to missing data.');
            }

            // Save all issues to files and remove them from the rules, except first n issues
            $this->flushRuleIssues($rule, true);
        }

        $result = $this->getValidationResult($rules);
        file_put_contents(
            "$this->cacheDir/result.json",
            json_encode($result, Config::$jsonFlags),
        );

        $status->stage = 'finished';
        $status->progress = 1;
        $status->secondsRemaining = 0;
        $status->runtime = microtime(true) - $this->startTimestamp;
        $this->saveStatus();
    }

    /**
     * Add a validation error.
     *
     * These errors are immediately shown to the user
     * during validation.
     */
    protected function addError(string $message, int|string ...$values): void
    {
        $error = (object) ['message' => $message];

        if ($values) {
            $error->values = $values;
        }

        $this->status->errors[] = &$error;

        $this->saveStatus();
    }

    /**
     * Download and cache a single XML file from an OAI API.
     */
    protected function cacheOaiDcXml(string $oaiVerb, string $resumptionToken = ''): object|bool
    {
        $startTimestamp = microtime(true);

        $xmlCacheDir = "$this->cacheDir/xml/$oaiVerb";

        if (! file_exists($xmlCacheDir)) {
            mkdir($xmlCacheDir, 0700, true);
        }

        $file = $this->downloadOaiXml($oaiVerb, $resumptionToken);

        if (! $file || ! $file->xml->{$oaiVerb}) {
            return false;
        }

        // Get next free number
        $i = count(scandir($xmlCacheDir) ?: []) - 2 + 1; // "- 2" to exclude . and ..
        $xmlCacheFilename = "$xmlCacheDir/$i.xml";
        file_put_contents($xmlCacheFilename, $file->content);

        if ($oaiVerb === 'ListRecords') {
            $recordsBatchSize = count($file->xml->{$oaiVerb}?->record ?? []);

            $recordsCount = 0;
            foreach ($file->xml->{$oaiVerb}->record as $record) {
                if ($record->header && (string) $record->header['status'] !== 'deleted') {
                    $recordsCount++;
                }
            }
        }

        if ($file->xml->{$oaiVerb}->resumptionToken) {
            $newResumptionToken = (string) $file->xml->{$oaiVerb}->resumptionToken;
            $completeListSize = (int) $file->xml->{$oaiVerb}->resumptionToken['completeListSize'];
        }

        return (object) [
            'resumptionToken' => $newResumptionToken ?? null,
            'completeListSize' => $completeListSize ?? null,
            'recordsBatchSize' => $recordsBatchSize ?? null,
            'recordsCount' => $recordsCount ?? 0,
            'duration' => microtime(true) - $startTimestamp,
            'content' => $file->content,
        ];
    }

    /**
     * Save status to file and remove shared memory.
     */
    protected function cleanUp(): void
    {
        $this->saveStatus();

        if ($this->shm) {
            if (shm_has_var($this->shm, 0)) {
                shm_remove_var($this->shm, 0);
            }

            shm_remove($this->shm);
        }
    }

    /**
     * Flush issues of a given rule.
     *
     * Removes the latest or all issues (if $all if true) from a rule and saves
     * them to a JSON file.
     */
    protected function flushRuleIssues(Rule $rule, bool $all = false): void
    {
        $ruleIssuesDir = "$this->cacheDir/issues/$rule->id";
        while ($issues = $rule->spliceIssues($all)) {
            if (! file_exists($ruleIssuesDir)) {
                mkdir($ruleIssuesDir, 0700, true);
            }

            $existingFilenames = glob("$ruleIssuesDir/*.json", GLOB_NOSORT);
            $number = 1;

            if ($existingFilenames) {
                natsort($existingFilenames);
                $highestNumber = (int) basename(end($existingFilenames), '.json');
                $number = $highestNumber + 1;
            }

            file_put_contents(
                "$ruleIssuesDir/$number.json",
                json_encode($issues, Config::$jsonFlags),
            );
        }
    }

    /**
     * Get the number of all cached XML files for the current validation.
     */
    protected function getCachedXmlFilesCount(): int
    {
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(
                "$this->cacheDir/xml",
                \RecursiveDirectoryIterator::SKIP_DOTS,
            ),
        );

        return iterator_count($iterator);
    }

    /**
     * Get a HTML string of an OAI API request link.
     */
    protected function getOaiLink(string $oaiVerb, string $resumptionToken = ''): string
    {
        $url = "{$this->status->oaiApiUrl}?verb=$oaiVerb";

        if ($resumptionToken) {
            $url .= "&resumptionToken=$resumptionToken";
        } elseif (in_array($oaiVerb, ['GetRecord', 'ListIdentifiers', 'ListRecords'])) {
            $url .= '&metadataPrefix=oai_dc';
        }

        $url = htmlspecialchars($url, ENT_QUOTES);

        return "<a href='$url'>$url</a>";
    }

    /**
     * Get the validation results.
     *
     * @param  object[]  $rules
     */
    protected function getValidationResult(array $rules): object
    {
        $result = (object) [
            'pointsMax' => 0,
            'points' => 0,
        ];

        $nestedRules = [];
        foreach ($rules as $rule) {
            $ruleResult = $rule->getResult();

            // Skip child rules
            if (preg_match(Config::$childRuleIdRegex, $ruleResult->id)) {
                continue;
            }

            if (! empty($ruleResult->weight)) {
                $result->pointsMax += $ruleResult->weight;
                $result->points += $ruleResult->points; // @phpstan-ignore-line
            }

            $childRules = [];
            foreach ($rules as $rule) {
                if (! preg_match(Config::$childRuleIdRegex, $rule->id)
                    || ! str_starts_with($rule->id, $ruleResult->id)
                ) {
                    // Not a child
                    continue;
                }

                $childResult = $rule->getResult();

                if (empty($childResult->manual)) {
                    $result->pointsMax += $childResult->weight;
                    $result->points += $childResult->points;
                }

                $childRules[] = $childResult;
            }

            if ($childRules) {
                $count = $ruleResult->manual ? 0 : 1;
                foreach ($childRules as $childRule) {
                    if ($childRule->manual) {
                        continue;
                    }

                    $count++;
                    $ruleResult->passedRatio += $childRule->passedRatio;
                }

                $ruleResult->childRules = $childRules;
                $ruleResult->passedRatio = $count
                    ? $ruleResult->passedRatio / $count
                    : 0;
            }

            $nestedRules[] = $ruleResult;
        }

        $result->rulesetVersion = Config::$rulesetVersion;
        $result->rules = array_values($nestedRules);
        $result->timestamp = microtime(true);
        $result->score = $result->pointsMax // @phpstan-ignore-line
            ? floor($result->points / $result->pointsMax * 1000) / 10
            : 0;

        return $result;
    }

    /**
     * Get all available rules.
     *
     * Reads the ruleset configuration, and for every rule, attaches the
     * configuration and calls setup().
     *
     * @return Rule[]
     */
    protected function getRules(): array
    {
        $json = file_get_contents(Config::$dataDir . '/rulesets/' . Config::$rulesetVersion . '.json');
        $ruleConfigs = (array) json_decode($json);

        if (! $ruleConfigs) {
            trigger_error('Error parsing ruleset JSON', E_USER_ERROR);
        }

        $rules = [];
        foreach ($ruleConfigs as $ruleConfig) {
            $id = $ruleConfig->id;
            $weight = $ruleConfig->weight ?? 0;
            $className = empty($ruleConfig->manual) ? $id : 'Rule';

            $rule = new (__NAMESPACE__ . "\\$className")($id, $weight, $this);
            $rule->setup();

            $rules[] = $rule;
        }

        return $rules;
    }

    /**
     * Sanitize a URL to be used as directory name.
     */
    protected function urlToDirname(string $string): string
    {
        $string = (string) preg_replace('#^\w+://#', '', $string);
        $string = (string) preg_replace('/[^\p{L}0-9~!@#$%&()_+=\[\];:,.-]/', '', $string);
        $string = trim($string, '.');

        return $string;
    }

    /**
     * Write the current status to the shared memory.
     *
     * If $savedToFiles is true, it is also saved to status.json.
     */
    protected function saveStatus(bool $savedToFile = true): void
    {
        if (empty($this->status) || empty($this->status->processId)) {
            trigger_error('Trying to write shared memory before status is initialized', E_USER_ERROR);
        }

        if (! $this->shm) {
            $this->shm = shm_attach($this->status->processId) ?: null;
        }

        if (! $this->shm) {
            trigger_error('Error attaching shared memory', E_USER_ERROR);
        }

        shm_put_var($this->shm, 0, $this->status);

        if ($savedToFile && ! defined('TESTING')) {
            if (! file_exists($this->cacheDir)) {
                $success = mkdir($this->cacheDir, 0700, true);

                if (! $success) {
                    trigger_error('Error creating cache directory', E_USER_ERROR);
                }
            }

            file_put_contents(
                "$this->cacheDir/status.json",
                json_encode($this->status, Config::$jsonFlags),
            );
        }
    }

    /**
     * Run applicable rules on a single cached XML file
     *
     * @param  Rule[]  $applicableRules
     */
    protected function validateFile(
        string $oaiVerb,
        array &$applicableRules,
        string $filename,
        bool $isLast,
    ): void {
        $content = file_get_contents($filename);

        if ($content === false) {
            $this->addError(
                'Error reading $1 file $2',
                $oaiVerb,
                basename($filename, '.xml'),
            );

            return;
        }

        $xml = @simplexml_load_string($content);

        if (! $xml) {
            $this->addError(
                'Invalid XML for $1 ($2)',
                $oaiVerb,
            );

            return;
        }

        if ($oaiVerb === 'ListRecords') {
            foreach ($xml->ListRecords->record as $record) {
                if ($record->header &&
                    (string) $record->header['status'] !== 'deleted'
                ) {
                    $this->status->recordsChecked++;
                }
            }
        }

        foreach ($applicableRules as $rule) {
            if (isset($rule->finished)) {
                continue;
            }

            $rule->run($xml, $isLast);

            $this->flushRuleIssues($rule);
        }
    }
}
