import { ref, onUnmounted } from 'vue'

import { cache } from './cache'
import { errorHandler } from './error'

export const loading = ref(false)

export function createEventSource (url) {
  const eventSource = new EventSource(url)

  loading.value = true

  eventSource.addEventListener('message', async () => {
    loading.value = false
  })

  eventSource.addEventListener('error', () => {
    eventSource.close()
    loading.value = false
    errorHandler.set('Server error')
  })

  // Without this, EventSource throws an error on reload
  window.addEventListener('beforeunload', closeEventSource)

  onUnmounted(() => {
    eventSource.close()
    loading.value = false
    window.removeEventListener('beforeunload', closeEventSource)
  })

  function closeEventSource () {
    eventSource.close()
  }

  return eventSource
}

export async function fetchJson (url, options = {}, useCache = true) {
  const cacheKey = url + JSON.stringify(options)

  if (useCache && cache.get(cacheKey)) {
    return cache.get(cacheKey)
  }

  loading.value = true

  const response = await fetch(url, options).catch(() => {
    errorHandler.set('Network error')
  })

  loading.value = false

  if (response?.ok) {
    try {
      const content = await response.json()

      if (useCache) {
        cache.set(cacheKey, content)
      }

      return content
    } catch {
      errorHandler.set('Received invalid JSON')

      return false
    }
  } else if (response?.body) {
    try {
      const error = await response.json()
      errorHandler.set(error.error || error)
    } catch {
      errorHandler.set('Server error')
    }
  }

  return false
}
