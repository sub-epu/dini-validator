<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_9_3_2Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListIdentifiers/empty');
        $this->assertEquals(
            'Schema validation errors in <a>$1</a>:<br>$2',
            $result->issues[0]->text,
        );
    }
}
