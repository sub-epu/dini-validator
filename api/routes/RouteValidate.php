<?php

declare(strict_types=1);

namespace Dini\Validator;

class RouteValidate extends Route
{
    protected string $method = 'POST';

    public function sendResultMail(string $to, string $oaiApiUrl, string $taskId): bool|object
    {
        if (empty(Config::$mail)) {
            return false;
        }

        $mailTemplate = file_get_contents(Config::$mail['templateFile']);

        if ($mailTemplate === false) {
            trigger_error('Error loading result mail template', E_USER_WARNING);

            return false;
        }

        $shortOaiApiUrl = (string) preg_replace('#^\w+://#', '', $oaiApiUrl);
        $subject = str_replace('$oaiApiUrl', $shortOaiApiUrl, Config::$mail['subject']);

        $message = $mailTemplate;
        $message = str_replace('$oaiApiUrl', $oaiApiUrl, $message);
        $message = (string) preg_replace(
            '/\$resultUrl(\/[a-z]{2})?/',
            "https://{$_SERVER['SERVER_NAME']}$1/{$taskId}",
            $message,
        );

        $additionalHeaders = [
            'Mime-Version' => '1.0',
            'Content-type' => 'text/plain;charset=utf-8',
            'From' => Config::$mail['from'],
        ];

        if (isset(Config::$mail['replyTo'])) {
            $additionalHeaders['Reply-To'] = Config::$mail['replyTo'];
        }

        $sent = defined('TESTING')
            ? true
            : mail(
                $to,
                $subject,
                $message,
                $additionalHeaders,
            );

        if (! $sent) {
            trigger_error('Sending result mail failed', E_USER_WARNING);

            return false;
        }

        return (object) [
            'to' => $to,
            'subject' => $subject,
            'message' => $message,
            'sent' => $sent,
        ];
    }

    public function serve(): void
    {
        $payload = $this->readJson('php://input') ?? null;

        if (! $payload) {
            $this->fail('Invalid request', 400);
        }

        $oaiApiUrl = $payload?->oaiApiUrl ?? '';

        if (! filter_var($oaiApiUrl, FILTER_VALIDATE_URL)
            || ! preg_match('#^https?://#', $oaiApiUrl)
            || strlen($oaiApiUrl) > 200
            || str_contains($oaiApiUrl, '?')
        ) {
            $this->fail('Invalid OAI API URL', 400);
        }

        $email = $payload?->email ?? '';
        if (! filter_var($email, FILTER_VALIDATE_EMAIL)
            || strlen($email) > 200
        ) {
            $this->fail('Invalid email address', 400);
        }

        $recordsLimit = $payload?->recordsLimit ?? null;

        if (! in_array($recordsLimit, Config::$recordsLimitOptions, true)) {
            $this->fail('Invalid max records value', 400);
        }

        // Check if validation for this API is already in progress, clean up XML caches
        $cacheDirs = glob(Config::$cacheDir . '/*', GLOB_NOSORT) ?: [];
        foreach ($cacheDirs as $dir) {
            $status = $this->readJson("$dir/status.json");

            if (! $status) {
                trigger_error("Error loading status file: $dir/status.json");

                continue;
            }

            if (strtolower($status->oaiApiUrl) === strtolower($oaiApiUrl)
                && $status->progress !== 1
            ) {
                // Validation for this OAI API is currently running
                $this->sendJson([
                    'busy' => true,
                    'taskId' => $status->taskId,
                ]);

                return;
            }

            $this->removeStaleXmlCache($dir);
        }

        // Exit if free disk space is below 5 GB
        // Validating the OAI API of a large repo takes up to 3 GB
        if (disk_free_space('.') < Config::$minFreeDiskSpace) {
            trigger_error('Low disk space');
            $this->fail('Server is low on disk space, cannot start validation', 507);
        }

        $validator = new Validator($oaiApiUrl, $recordsLimit);
        $validationStatus = $validator->getStatus();

        // Output task ID but keep script running. This combination of ob_start and
        // multiple flushes is the only one that works reliably on different servers.
        ignore_user_abort(true);
        set_time_limit(0);

        header("Connection: close\r\n");
        header("Content-Encoding: none\r\n");
        ob_start();
        $this->sendJson(['taskId' => $validationStatus->taskId]);
        header('Content-Length: ' . ob_get_length());
        ob_end_flush();
        @ob_flush();
        flush();

        if (function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        }

        if ($validationStatus->stage === 'init' || $validationStatus->stage === 'finished') {
            $validator->download();

            if ($validationStatus->stage !== 'failed') {
                $validator->validate();
                $this->sendResultMail($email, $oaiApiUrl, $validationStatus->taskId);
            }
        }
    }

    protected function removeDir(string $dir): void
    {
        if (! is_dir($dir)) {
            return;
        }

        $names = scandir($dir) ?: [];
        foreach ($names as $name) {
            if ($name === '.' || $name === '..') {
                continue;
            }

            if (is_dir("$dir/$name")) {
                $this->removeDir("$dir/$name");
            } else {
                unlink("$dir/$name");
            }
        }

        rmdir($dir);
    }

    protected function removeStaleXmlCache(string $dir): void
    {
        $threshold = strtotime(Config::$xmlCacheMaxAge);

        if ($threshold > filemtime($dir)) {
            $this->removeDir("$dir/xml");
        }
    }
}
