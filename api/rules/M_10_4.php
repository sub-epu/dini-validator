<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_10_4 extends Rule
{
    public string $oaiVerb = 'Identify';

    public function check($xml, $isLastBatch): void
    {
        if (empty($xml->Identify->deletedRecord)) {
            $this->addFatalIssue(
                'Identify',
                '<code>$1</code> is missing in <a>$2</a>',
                'deletedRecord',
                'Identify',
            );

            return;
        }
        if (! in_array($xml->Identify->deletedRecord, ['no', 'persistent', 'transient'])) {
            $this->addFatalIssue(
                'Identify',
                '<code>$1</code> is invalid in <a>$2</a>',
                'deletedRecord',
                'Identify',
            );

            return;
        }
    }
}
