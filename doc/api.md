# API

The backend is a JSON REST API which supplies data to the frontend.

To update progress during a validation with minimal overhead, the `watch` route uses [server-sent events](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events) with JSON data instead plain JSON.

The API is English-only, translations are entirely done in the frontend.

## Directory Structure

Base directory: `api`

- `classes`
    The validator’s core classes.
- `config`
    Global backend configuration.
- `routes`
    Classes for available API routes. Each file corresponds to an API route.
- `rules`
    Classes for all rules defined in the current ruleset that are validated programmatically.
- `test`
    Additional files for running unit tests, including a mock OAI-PMH API.

## Validation Run

For every validation, these steps are executed:

1. An `Identify` request is sent to the supplied OAI API URL. If the response is valid, the XML is saved. Otherwise the validation is cancelled.
2. If the first response was valid, requests are sent for each of the OAI verbs `ListIdentifiers`, `ListMetadataFormats`, `ListRecords`, and `ListSets`. The responses are saved as XML files. If any of these verbs have a `resumptionToken`, subsequent requests are sent and saved as XML, with the exception of `ListRecords`, where only so many batches are downloaded until the user-set record limit is fulfilled. The progress is displayed, so are errors, but the validation is not aborted.
3. For each downloaded file, all applicable rules are run, see [rulesets](./rulesets.md).
4. Once all files have been checked, the result is displayed.
