<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_10_3 extends Rule
{
    public string $oaiVerb = 'ListSets';

    public function check($xml, $isLastBatch): void
    {
        foreach ($xml->ListSets->set as $set) {
            if (str_starts_with((string) $set->setSpec, 'doc-type:')) {
                $this->finish();

                return;
            }
        }

        if ($isLastBatch) {
            $this->addFatalIssue(
                'ListSets',
                'Sets for document types are missing in <a>ListSets</a>.',
            );

            return;
        }
    }
}
