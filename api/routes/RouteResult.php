<?php

declare(strict_types=1);

namespace Dini\Validator;

class RouteResult extends Route
{
    public function serve(?bool $issuesIncluded = null): void
    {
        $taskId = $this->sanitizeTaskId($_GET['taskId'] ?? '');
        $issuesIncluded ??= isset($_GET['issues']);

        $cacheDir = Config::$cacheDir . "/$taskId";

        if (! $taskId || ! is_dir($cacheDir)) {
            $this->fail('Unknown task ID', 404);
        }

        $resultFilename = "$cacheDir/result.json";

        // Result file may need a few seconds to be written after validation is complete
        $retriesLeft = 5;
        while (! is_readable($resultFilename) && $retriesLeft) {
            $retriesLeft--;
            sleep(1);
        }

        $status = $this->readJson("$cacheDir/status.json");

        if (! $status) {
            $this->fail('Error reading status', 500);
        }

        $result = $this->readJson("$cacheDir/result.json");

        if (! $result) {
            $this->fail('Error reading result', 500);
        }

        $rulesetVersion = $result->rulesetVersion ?? Config::$rulesetVersion;
        $ruleset = (array) $this->readJson(Config::$dataDir . "/rulesets/$rulesetVersion.json");

        if (! $ruleset) {
            $this->fail('Error reading ruleset', 500);
        }

        // Remove data not relevant for the user
        unset($status->processId);
        unset($status->progress);
        unset($status->secondsRemaining);
        unset($status->stage);

        if (empty($status->errors)) {
            unset($status->errors);
        }

        foreach ($result->rules as &$rule) {
            $this->addRuleDetails($rule, $ruleset);

            if ($issuesIncluded) {
                $rule->issues = $this->getFirstIssues($cacheDir, $rule->id);
            }

            if (isset($rule->childRules)) {
                foreach ($rule->childRules as &$childRule) {
                    $this->addRuleDetails($childRule, $ruleset);

                    if ($issuesIncluded) {
                        $childRule->issues = $this->getFirstIssues($cacheDir, $childRule->id);
                    }
                }
            }
        }

        $mergedResultArray = array_merge((array) $result, (array) $status);
        ksort($mergedResultArray);
        $mergedResult = (object) $mergedResultArray;

        $this->sendJson($mergedResult);
    }

    /**
     * @param  mixed[]  $ruleset
     */
    private function addRuleDetails(object &$rule, array $ruleset): void
    {
        $details = current(array_filter($ruleset, fn ($r) => $r->id === $rule->id));

        if (! $details) {
            $this->fail('Incomplete ruleset', 500);
        }

        $rule->key = $details->key;
        $rule->title = $details->title;
        $rule->description = $details->description;
    }

    /**
     * @return mixed[]
     */
    private function getFirstIssues(string $cacheDir, string $ruleId, int $limit = 10): array
    {
        $firstIssuesFile = "$cacheDir/issues/$ruleId/1.json";

        if (is_readable($firstIssuesFile)) {
            $issues = (array) $this->readJson($firstIssuesFile);

            return array_slice($issues, 0, $limit);
        }

        return [];
    }
}
