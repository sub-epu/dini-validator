import JSConfetti from 'js-confetti'

export async function confetti () {
  // No confetti for users who dislike motion
  if (window.matchMedia('(prefers-reduced-motion: reduce)').matches) {
    return
  }

  const jsConfetti = new JSConfetti()
  await jsConfetti.addConfetti({
    confettiColors: ['#344999', '#68ba80', '#f0821d', '#d72f89'], // DINI colors
  })

  document.querySelector('canvas').remove()
}
