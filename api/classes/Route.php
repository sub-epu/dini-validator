<?php

declare(strict_types=1);

namespace Dini\Validator;

class Route
{
    /**
     * Content-type sent to the browser
     */
    protected string $contentType = 'application/json';

    /**
     * HTTP request method for this route
     */
    protected string $method = 'GET';

    /**
     * Set common HTTP access-control and content-type headers.
     */
    public function setHttpHeader(): void
    {
        header('Access-Control-Allow-Headers: accept, authorization, content-type, origin');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: $this->method");
        header("Content-Type: $this->contentType");
    }

    /**
     * Fail the request, i.e. send an HTTP error code and an error message,
     * and terminate the script.
     */
    protected function fail(string $message, int $statusCode = 500): never
    {
        http_response_code($statusCode);
        $this->sendJson(['error' => $message]);
        exit;
    }

    /**
     * Read and decode a JSON file.
     */
    protected function readJson(string $filename): mixed
    {
        $string = file_get_contents($filename);

        if ($string === false) {
            return false;
        }

        $result = json_decode($string);

        return $result;
    }

    /**
     * Sanitize a rule ID to be used as class name.
     */
    protected function sanitizeRuleId(string $ruleId): ?string
    {
        return preg_replace('#[^a-zA-Z0-9_]#', '', $ruleId);
    }

    /**
     * Sanitize a task ID to be used as directory name.
     */
    protected function sanitizeTaskId(string $taskId): ?string
    {
        return preg_replace('#[^\p{L}0-9.,;!=\[\]@%~+-]#', '', $taskId);
    }

    /**
     * Output payload to the browser as JSON.
     */
    protected function sendJson(mixed $payload): void
    {
        $json = json_encode($payload, Config::$jsonFlags);

        if ($json) {
            echo $json;
        } else {
            $this->fail('Encoding error: ' . json_last_error_msg());
        }
    }

    /**
     * Serve route
     *
     * For being overridden in child routes.
     */
    protected function serve(): void
    {
    }
}
