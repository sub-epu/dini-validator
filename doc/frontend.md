# Frontend

The frontend is based on [Vue.js 3](https://vuejs.org/) with [Single File Components](https://vuejs.org/guide/introduction.html#single-file-components) (SFCs). Only the new [Composition API](https://vuejs.org/guide/introduction.html#api-styles) is used.

Each SFC contains three sections in the same order:

- `<script>` for the JavaScript code
- `<template>` for the HTML template, written in [Pug](https://pugjs.org/)
- `<style>` for the stylesheet, written in [SCSS](https://sass-lang.com/), may be omitted if not required

## Directory Structure

Base directory: `frontend`

- `components`
    All SFCs which are not a view (see below).
- `modules`
    [JavaScript modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules). Each module most provide at least one named `export`. Unnamed exports should be avoided.
- `plugins`
    Custom [Vue plugins](https://vuejs.org/guide/reusability/plugins.html).
- `styles`
    This directory contains SCSS with common styles, which complement styles in SFCs.
- `translations`
    Translation files, see [translation plugin](../frontend/plugins/i18n.js).
- `views`
    Components which serve as frontend routes, see [configuration for Vue Router](../frontend/router.js). These components may import any components from `components`, but not vice versa.

## Build Tools

The frontend is built with [Vite](https://vitejs.dev/), the new Vue build tool. See [vite.config.js](../vite.config.js) for configuration options, the [Vite Configuration Reference](https://vitejs.dev/config/) and `scripts` in [package.json](../package.json) for available build scripts.

## Styles

The frontend is fully responsive, and the stylesheet contains a dark mode. Make sure to test on different screen sizes and with both light and dark mode when making changes.

## Testing

To run a validation locally without accessing a remote OAI-PMH API, enter `http://localhost/api/test/mock-oai-api/` as OAI API URL.

To test intermediate views like progress and some special states, query parameters may be added to the local URL (usually `http://localhost:5173`).

- [`busy`](http://localhost:5173?busy) displays the already-running view.
- [`error`](http://localhost:5173?error) displays a generic error popup. Use `error=<message>` to display a custom message, e.g. `error=This is a custom error`.
- [`loading`](http://localhost:5173?loading) displays the loading spinner.
- [`nocache`](http://localhost:5173?nocache) bypasses the Session Storage, wherein all results and issues are cached.

Some parameters only have effect in the progress and result view, i.e. the URL has a path that does not correspond to another view, e.g. `http://localhost:5173/xyz`:

- [`failed`](http://localhost:5173/xyz?failed) displays the validation-failed view.
- [`progress`](http://localhost:5173/xyz?progress) displays the progress view with a random progress value. Use `progress=<value>` to set the progress, e.g. `progress=.5` for 50&nbsp;%.

## Translations

Translations are entirely done in the frontend, the API is English-only. There are two ways to translate texts:

1. Using the `i18n` plugin and its `$t` function. This is the way to go for all messages coming from the API, and for short strings that appear in multiple places. The translation is defined in a JSON file matching the language code, e.g. `de.json` for German, located either in `frontend/translations` (for direct `import`) or in `public/translations` (for loading on demand). Strings can contain placeholders, i.e. `$` followed by a digit, like `$1` and `$2`. Also see [frontend/plugins/i18n.js](../frontend/plugins/i18n.js).

2. Using the `VTranslation` Vue component. Translations are written directly into templates, which is suitable for longer texts that only appear in a single location, especially when those texts contain HTML. The `VTranslation` component has one value-less prop, denoting the language, which has to match a key of the `languages` set in [frontend/config.js](../frontend/config.js).

    Example:

    ```vue
    <template lang="pug">
      VTranslation(en)
        p This is a paragraph in English.
      VTranslation(de)
        p Das ist der gleiche Absatz auf Deutsch.
    </template>
    ```
