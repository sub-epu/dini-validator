import { nextTick } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'

import { errorHandler } from './modules/error'
import { setTitle } from './modules/meta'

import HomeView from './views/HomeView.vue'
import ContactView from './views/ContactView.vue'
import LegalView from './views/LegalView.vue'
import ValidationView from './views/ValidationView.vue'
import NotFoundView from './views/NotFoundView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      name: 'home',
      path: '/:lang([a-z]{2})?/',
      component: HomeView,
    },
    {
      name: 'contact',
      path: '/:lang([a-z]{2})?/contact',
      component: ContactView,
    },
    {
      name: 'legal',
      path: '/:lang([a-z]{2})?/legal',
      component: LegalView,
    },
    {
      name: 'validation',
      path: '/:lang([a-z]{2})?/:taskId([^/]{1,})',
      component: ValidationView,
    },
    {
      name: '404',
      path: '/:lang([a-z]{2})?/:pathMatch(.*)?',
      component: NotFoundView,
    },
  ],
  scrollBehavior (to, from, savedPosition) {
    if (to.name === from.name) {
      return
    }

    if (savedPosition) {
      return savedPosition
    }

    return {
      behavior: 'instant',
      top: 0,
    }
  },
})

router.afterEach(() => {
  errorHandler.clear()
  nextTick(setTitle)
})

export default router
