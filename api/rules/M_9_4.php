<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_9_4 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    public function checkRecord($record): void
    {
        if (! $record->header->datestamp) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is missing in <a>$2</a>',
                'datestamp',
                (string) $record->header->identifier,
            );

            return;
        }

        if (! date_create((string) $record->header->datestamp)) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is invalid in <a>$2</a>',
                'datestamp',
                (string) $record->header->identifier,
            );
        }
    }
}
