let scrollTimeout
let scrollY = 0

export function clearScrollTimeout () {
  clearTimeout(scrollTimeout)
}

export function scrollToHash (event) {
  const hash = event.newURL.split('#')[1]
  if (hash) {
    // eslint-disable-next-line unicorn/prefer-query-selector
    document.getElementById(hash)?.scrollIntoView()
  } else {
    window.scroll({ top: 0 })
  }
}

export function updateUrlHash (elements) {
  if (!elements) {
    return
  }

  scrollY = window.scrollY

  clearTimeout(scrollTimeout)
  scrollTimeout = setTimeout(() => {
    if (window.scrollY !== scrollY) {
      return
    }

    if (!scrollY) {
      // Remove hash
      history.replaceState(history.state, '', ' ')
      return
    }

    for (const element of elements) {
      if (element.offsetTop - scrollY > 0) {
        if (location.hash.slice(1) !== element.id) {
          history.replaceState(history.state, '', `#${element.id}`)
        }

        break
      }
    }
  }, 200)
}
