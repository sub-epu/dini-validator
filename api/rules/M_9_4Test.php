<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_9_4Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListRecords/invalid-datestamps');
        $this->assertEquals(3, $result->issuesCount);

        $this->assertEquals(
            '<code>$1</code> is missing in <a>$2</a>',
            $result->issues[0]->text,
        );
        $this->assertEquals(
            'datestamp',
            $result->issues[0]->values[0],
        );
        $this->assertEquals(
            'oai:1',
            $result->issues[0]->values[1],
        );

        $this->assertEquals(
            '<code>$1</code> is invalid in <a>$2</a>',
            $result->issues[1]->text,
        );
        $this->assertEquals(
            'datestamp',
            $result->issues[1]->values[0],
        );
        $this->assertEquals(
            'oai:2',
            $result->issues[1]->values[1],
        );

        $this->assertEquals(
            '<code>$1</code> is invalid in <a>$2</a>',
            $result->issues[2]->text,
        );
        $this->assertEquals(
            'datestamp',
            $result->issues[2]->values[0],
        );
        $this->assertEquals(
            'oai:3',
            $result->issues[2]->values[1],
        );
    }
}
