<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_9_1 extends Rule
{
    public string $oaiVerb = 'Identify';

    public function check($xml, $isLastBatch): void
    {
        if ((string) $xml->Identify?->protocolVersion !== '2.0') {
            $this->addFatalIssue(
                'Identify',
                '<a>Identify</a> is invalid',
            );

            return;
        }
    }
}
