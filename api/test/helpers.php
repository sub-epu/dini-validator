<?php

declare(strict_types=1);

/**
 * Get the parsed text for an issue, where all placeholders are replaced with values.
 */
function getIssueText(object $issue): string
{
    return preg_replace_callback(
        '#\$(\d)#',
        fn ($matches) => $issue->values[(int) $matches[1] - 1],
        $issue->text,
    );
}

/**
 * Run a rule isolated for unit tests.
 *
 * @param  mixed[]  $overriddenProperties
 */
function runRule(string $filename, array $overriddenProperties = []): object
{
    $backtrace = debug_backtrace();
    $testClassName = $backtrace[1]['class'];
    $testedClassName = str_replace('Test', '', $testClassName);

    $validator = new \Dini\Validator\Validator('http://localhost:8002');
    $rule = new $testedClassName('', 0, $validator);
    $rule->setup();

    foreach ($overriddenProperties as $key => $value) {
        $rule->{$key} = $value;
    }

    $content = file_get_contents(__DIR__ . "/xml/$filename.xml");
    $xml = simplexml_load_string($content);

    $rule->run($xml);

    return $rule->getResult(true);
}
