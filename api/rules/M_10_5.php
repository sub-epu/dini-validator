<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_10_5 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    public $savedResultFile = '';

    public function setup(): void
    {
        $this->savedResultFile = "{$this->validator->cacheDir}/xml/ListRecords/2.xml";
    }

    public function check($xml, $isLastBatch): void
    {
        if (empty($xml->ListRecords->resumptionToken)) {
            $this->finish('Not relevant due to small repository size');

            return;
        }

        if (! file_exists($this->savedResultFile)) {
            $this->addFatalIssue('ListRecords', 'XML file not found');

            return;
        }

        $savedContents = file_get_contents($this->savedResultFile);
        $savedXml = @simplexml_load_string((string) $savedContents);

        $newResult = $this->validator->downloadOaiXml('ListRecords', (string) $xml->ListRecords->resumptionToken);
        $newXml = @simplexml_load_string((string) $newResult->content);

        if (! $savedXml) {
            $this->addIssue(
                "ListRecords&resumptionToken={$xml->ListRecords->resumptionToken}",
                'Invalid response to <a>$1</a>',
                'ListRecords (1)',
            );
        } elseif (! $newXml) {
            $this->addIssue(
                "ListRecords&resumptionToken={$xml->ListRecords->resumptionToken}",
                'Invalid response to <a>$1</a>',
                'ListRecords (2)',
            );
        } else {
            $index = 0;
            foreach ($savedXml->ListRecords->record as $record) {
                if (
                    ! $newXml->ListRecords?->record
                    || json_encode($record) !== json_encode($newXml->ListRecords->record[$index])
                ) {
                    $this->addIssue(
                        "ListRecords&resumptionToken={$xml->ListRecords->resumptionToken}",
                        'Consecutive <a>ListRecords</a> requests do not match.',
                    );
                    break;
                }

                $index++;
            }
        }

        $this->finish();
    }
}
