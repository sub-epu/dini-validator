<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_9_3Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListRecords/about-without-provenance');
        $this->assertEquals(
            '<code>about</code> is set, but <code>provenance</code> is missing in <a>about-without-provenance</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
