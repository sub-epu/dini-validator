export default {
  // URL of the validator API
  // Protocol and domain may be omitted if same as frontend.
  apiUrl:
    process.env.NODE_ENV === 'development'
      ? '//localhost:8001/public/api'
      : '/api',

  // Default language
  // The value must match a key in "languages".
  defaultLang: 'en',

  // Available languages
  // Keys should have 2 characters and are used in URLs, and there must be
  // a translation file for every key other than the default language.
  languages: {
    en: 'English',
    de: 'Deutsch',
  },

  // Available options for how many records are checked, 0 means "all"
  // NOTE: This must also be set in the API config (api/config.php).
  recordsLimitOptions: [100, 500, 1000, 5000, 0],
}
