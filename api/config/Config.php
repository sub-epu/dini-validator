<?php

declare(strict_types=1);

namespace Dini\Validator;

class Config
{
    /**
     * Directory where downloaded XML and result JSON files are stored
     */
    public static string $cacheDir = __DIR__ . '/../../cache';

    /**
     * Regex to recognize child rule IDs, i.e. rules that belong to another rule
     */
    public static string $childRuleIdRegex = '/_.+_.+_/';

    /**
     * Directory where all static data is stored, e.g. rulesets
     */
    public static string $dataDir = __DIR__ . '/../data';

    /**
     * Maximum runtime of the XML downloader in seconds
     */
    public static float $downloadTimeLimit = 60 * 60 * 10; // 10 hours

    /**
     * Error display
     *
     * This will break the app even when just a notice appears and should only
     * be used for local testing. Never enable this on a production system!
     */
    public static bool $errorDisplay = false;

    /**
     * Location of the error log file
     *
     * Make sure the file is writable. Set to false to disable error logging.
     */
    public static bool|string $errorLog = __DIR__ . '/../../error.log';

    /**
     * Average time in seconds it takes on average to check a single batch
     *
     * This should usually be no higher than .1 seconds.
     */
    public static float $evaluationDurationPerBatch = .05;

    /**
     * Flags for json_encode, determining how JSON files are formatted
     */
    public static int $jsonFlags = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;

    /**
     * Options for the email which is sent when a validation completes
     *
     * To disable the mail function, set this to an empty array.
     *
     * @var string[]
     */
    public static array $mail = [
        /**
         * "From" header
         *
         * Make sure to use the correct domain or the email may get blocked
         */
        'from' => 'DINI Validator <noreply@validator.dini.de>',
        'replyTo' => 'gs@dini.de',

        /**
         * Mail subject
         *
         * Must satisfy http://www.faqs.org/rfcs/rfc2047.
         *
         * Available placeholders:
         * - $oaiApiUrl is replaced with the shortened validated OAI API URL
         */
        'subject' => 'DINI Validator: $oaiApiUrl',

        /**
         * Mail template file
         *
         * Available placeholders:
         * - $oaiApiUrl is replaced with the full validated OAI API URL
         * - $resultUrl is replaced with the result URL
         * - $resultUrl/xx is replaced with the localized URL;
         *   xx must match an available language code, e.g $oaiApiUrl/de
         */
        'templateFile' => __DIR__ . '/../data/mail-template.txt',
    ];

    /**
     * Minimum free disk space required to start a validation
     *
     * Validating the OAI API of the largest known repositories requires
     * about 2 GB of disk cache.
     */
    public static float $minFreeDiskSpace = 5e9; // 5 GB

    /**
     * Timeout for OAI API requests in seconds
     */
    public static float $oaiApiTimeout = 30;

    /**
     * Available options for how many records are checked, 0 means "all"
     *
     * NOTE: This must also be set in the frontend config (frontend/config.js)
     *
     * @var int[]
     */
    public static array $recordsLimitOptions = [100, 500, 1000, 5000, 0];

    /**
     * Current ruleset version
     *
     * Corresponding to a JSON file in rulesetsDir, e.g. rulesets/2022.json.
     */
    public static string $rulesetVersion = '2022';

    /**
     * Timezone
     *
     * See https://www.php.net/manual/en/timezones.php for a list of timezones.
     */
    public static string $timezone = 'Europe/Berlin';

    /**
     * “User-Agent” to be used for OAI API requests
     *
     * Some OAI APIs refuse to respond if User-Agent is missing.
     */
    public static string $userAgent = 'DINI Validator';

    /**
     * The max age of the XML cache written for each validation
     *
     * See https://www.php.net/manual/de/function.strtotime.php.
     */
    public static string $xmlCacheMaxAge = '-3 day';
}
