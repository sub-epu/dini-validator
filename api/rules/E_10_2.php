<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_10_2 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    public function check($xml, $isLastBatch): void
    {
        if (empty($xml->ListRecords->resumptionToken)) {
            $this->finish('Not relevant due to small repository size');

            return;
        }

        $recordsBatchSize = count($xml->ListRecords?->record ?? []);

        if ($recordsBatchSize < 100 || $recordsBatchSize > 500) {
            $this->addFatalIssue(
                'ListRecords',
                'The batch size of <a>$1</a> is $2.',
                'ListRecords',
                $recordsBatchSize,
            );

            return;
        }

        $this->finish();
    }
}
