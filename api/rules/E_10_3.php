<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_10_3 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    public function check($xml, $isLastBatch): void
    {
        if (empty($xml->ListRecords->resumptionToken)) {
            $this->finish('Not relevant due to small repository size');

            return;
        }

        if (empty($xml->responseDate)) {
            $this->addFatalIssue(
                'ListRecords',
                '<code>$1</code> is missing in <a>$2</a>',
                'responseDate',
                'ListRecords',
            );

            return;
        }

        $parts = explode('-', (string) $xml->responseDate);

        if (! checkdate((int) ($parts[1] ?? 1), (int) ($parts[2] ?? 1), (int) ($parts[0]))) {
            $this->addFatalIssue(
                'ListRecords',
                '<code>$1</code> is invalid in <a>$2</a>',
                'responseDate',
                'ListRecords',
            );

            return;
        }

        if (empty($xml->ListRecords->resumptionToken['expirationDate'])) {
            $this->addFatalIssue(
                'ListRecords',
                '<code>resumptionToken</code> without <code>expirationDate</code> in <a>ListRecords</a>',
            );

            return;
        }

        $parts = explode('-', (string) $xml->ListRecords->resumptionToken['expirationDate']);

        if (! checkdate((int) ($parts[1] ?? 1), (int) ($parts[2] ?? 1), (int) ($parts[0]))) {
            $this->addFatalIssue(
                'ListRecords',
                '<code>resumptionToken</code> with invalid <code>expirationDate</code> in <a>ListRecords</a>',
            );

            return;
        }

        $responseDate = date_create((string) $xml->responseDate);
        $expirationDate = date_create((string) $xml->ListRecords->resumptionToken['expirationDate']);

        $interval = $responseDate->diff($expirationDate);
        $days = $interval->format('%a');
        if ($days < 1) {
            $hours = $interval->format('%h');
            $this->addFatalIssue(
                'ListRecords',
                'The lifespan of the <code>resumptionToken</code> in <a>ListRecords</a> is $1.',
                $hours < 1 ? '&lt;&nbsp;1&nbsp;h' : "$hours&nbsp;h",
            );

            return;
        }

        $this->finish();
    }
}
