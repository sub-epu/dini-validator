<?php

declare(strict_types=1);

namespace Dini\Validator;

use DOMDocument;

class M_9_3_3 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    public function check($xml, $isLastBatch): void
    {
        $recordId = (string) $xml->ListRecords->record[0]?->header?->identifier ?? null;

        if (! $recordId) {
            $this->addFatalIssue(
                'ListRecords',
                'Invalid response to <a>$1</a>',
                'ListRecords',
            );

            return;
        }

        $result = $this->validator->downloadOaiXml('GetRecord', '', $recordId);

        if (! $result || ! $result->content) {
            $this->addFatalIssue(
                "GetRecord&identifier=$recordId",
                'Invalid response to <a>$1</a>',
                $recordId,
            );

            return;
        }

        $dom = new DOMDocument();
        $domCreated = @$dom->loadXML($result->content);

        // @codeCoverageIgnoreStart
        if (! $domCreated) {
            $this->addFatalIssue(
                "GetRecord&identifier=$recordId",
                'Invalid response to <a>$1</a>',
                $recordId,
            );

            return;
        }
        // @codeCoverageIgnoreEnd

        $dom->schemaValidate(Config::$dataDir . '/schemas/OAI-PMH.xsd');
        $xmlErrors = libxml_get_errors();
        libxml_clear_errors();
        $errorHtml = $this->xmlErrorsToHtml($xmlErrors);

        if ($errorHtml) {
            $this->addIssue(
                "GetRecord&identifier=$recordId",
                'Schema validation errors in <a>$1</a>:<br>$2',
                $recordId,
                $errorHtml,
            );
        }

        $this->finish();
    }
}
