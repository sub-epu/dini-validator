import { ref } from 'vue'

export const error = ref(undefined)

export const errorHandler = {
  set (message) {
    console.error(message)

    error.value = {
      message,
      timestamp: Date.now(),
    }
  },
  clear () {
    error.value = undefined
  },
}
