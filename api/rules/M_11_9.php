<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_11_9 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    public function checkRecord($record): void
    {
        $recordId = (string) $record->header?->identifier ?? null;

        if (! $recordId) {
            $this->addFatalIssue(
                'ListRecords',
                'Invalid response to <a>$1</a>',
                'ListRecords',
            );

            return;
        }

        // Without knowing if DataCite is actually supported, we just try to get a record
        $dataciteRecord = $this->validator->downloadOaiXml('GetRecord', '', $recordId, 'oai_datacite');
        $dataciteXml = @simplexml_load_string((string) $dataciteRecord->content);

        if (! $dataciteXml || $dataciteXml->error) {
            $this->finish('DataCite not available');

            return;
        }

        $oaiDc = $record->metadata?->children('oai_dc', true);

        if (! $oaiDc) {
            $this->addFatalIssue(
                "GetRecord&identifier=$recordId&metadataPrefix=oai_dc",
                '<code>$1</code> is invalid in <a>$2</a>',
                'metadata',
                $recordId,
            );

            return;
        }

        $oaiDatacite = $dataciteXml->GetRecord?->record?->metadata?->resource;

        if (! $oaiDatacite) {
            $this->addFatalIssue(
                "GetRecord&identifier=$recordId&metadataPrefix=oai_datacite",
                '<code>$1</code> is invalid in <a>$2</a>',
                'metadata',
                "$recordId (DataCite)",
            );

            return;
        }

        $dataciteIdentifier = $oaiDatacite->identifier;

        if (! preg_match('#^(http[s]://doi\.org/|doi:)?10\.\d{4,}/[^\s"<>]+$#', (string) $dataciteIdentifier)) {
            $this->addIssue(
                "GetRecord&identifier=$recordId&metadataPrefix=oai_datacite",
                'DataCite <code>identifier</code> is not a valid DOI in <a>$1</a>',
                "$recordId (DataCite)",
            );
        }

        // See https://schema.datacite.org/dc/
        $creators = $oaiDc->children('dc', true)->creator;
        $dataciteCreators = $oaiDatacite->creators;
        $i = 0;
        foreach ($creators as $creator) {
            if ((string) $creator !== (string) $dataciteCreators?->creator[$i++]?->creatorName) {
                $this->addIssue(
                    "GetRecord&identifier=$recordId&metadataPrefix=oai_datacite",
                    '<code>$1</code> does not match <code>$2</code> in <a>$3</a>',
                    'creator',
                    'dc:creator',
                    "$recordId (DataCite)",
                );
            }
        }

        $titles = $oaiDc->children('dc', true)->title;
        $dataciteTitles = $oaiDatacite->titles;
        $i = 0;
        foreach ($titles as $title) {
            if ((string) $title !== (string) $dataciteTitles?->title[$i++]) {
                $this->addIssue(
                    "GetRecord&identifier=$recordId&metadataPrefix=oai_datacite",
                    '<code>$1</code> does not match <code>$2</code> in <a>$3</a>',
                    'title',
                    'dc:title',
                    "$recordId (DataCite)",
                );
            }
        }

        $publishers = $oaiDc->children('dc', true)->publisher;
        $datacitePublishers = $oaiDatacite->publisher;
        $i = 0;
        foreach ($publishers as $publisher) {
            if ((string) $publisher !== (string) $datacitePublishers[$i++]) {
                $this->addIssue(
                    "GetRecord&identifier=$recordId&metadataPrefix=oai_datacite",
                    '<code>$1</code> does not match <code>$2</code> in <a>$3</a>',
                    'publisher',
                    'dc:publisher',
                    "$recordId (DataCite)",
                );
            }
        }

        $publicationYears = $oaiDc->children('dc', true)->issued;
        $datacitePublicationYears = $oaiDatacite->publicationYear;
        $i = 0;
        foreach ($publicationYears as $publicationYear) {
            if ((string) $publicationYear !== (string) $datacitePublicationYears[$i++]) {
                $this->addIssue(
                    "GetRecord&identifier=$recordId&metadataPrefix=oai_datacite",
                    '<code>$1</code> does not match <code>$2</code> in <a>$3</a>',
                    'publicationYear',
                    'dc:issued',
                    "$recordId (DataCite)",
                );
            }
        }

        $resourceTypes = $oaiDc->children('dc', true)->type;
        $dataciteResourceTypes = $oaiDatacite->resourceType;
        $i = 0;
        foreach ($resourceTypes as $resourceType) {
            if ((string) $resourceType !== (string) $dataciteResourceTypes[$i++]) {
                $this->addIssue(
                    "GetRecord&identifier=$recordId&metadataPrefix=oai_datacite",
                    '<code>$1</code> does not match <code>$2</code> in <a>$3</a>',
                    'resourceType',
                    'dc:type',
                    "$recordId (DataCite)",
                );
            }
        }

        $this->finish();
    }
}
