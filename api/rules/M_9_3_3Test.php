<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_9_3_3Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListRecords/no-records');
        $this->assertEquals(
            'Invalid response to <a>ListRecords</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/missing-record');
        $this->assertEquals(
            'Invalid response to <a>missing</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/invalid-identifier');
        $this->assertStringStartsWith(
            'Schema validation errors in <a>invalid</a>:',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
