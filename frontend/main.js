import { createApp } from 'vue'
import App from './App.vue'

import router from './router'

import i18n from './plugins/i18n'

import '@fontsource/barlow'
// NOTE: Italic font variants have to be imported one-by-one
import '@fontsource/barlow/300-italic.css'
import '@fontsource/noto-sans-mono'
import 'unfonts.css'

const app = createApp(App)

app.use(router)
app.use(i18n)

app.provide('i18n', app.config.globalProperties.$i18n)

app.mount('#app')
