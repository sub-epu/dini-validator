<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_10_1 extends Rule
{
    public string $oaiVerb = 'ListSets';

    private $statusTypes = [];

    public function setup(): void
    {
        $json = file_get_contents(Config::$dataDir . '/status-types.json');
        $this->statusTypes = json_decode($json);
    }

    public function check($xml, $isLastBatch): void
    {
        foreach ($xml->ListSets->set as $set) {
            if (in_array((string) $set->setSpec, $this->statusTypes)) {
                $this->finish();

                return;
            }
        }

        if ($isLastBatch) {
            $this->addFatalIssue(
                'ListSets',
                'Sets for publication status are missing in <a>ListSets</a>.',
            );

            return;
        }
    }
}
