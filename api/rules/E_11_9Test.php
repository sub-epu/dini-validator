<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_11_9Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListRecords/no-metadata');
        $this->assertEquals(
            '<code>metadata</code> is missing in <a>record-1</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/no-oaidc');
        $this->assertEquals(
            '<code>oai_dc</code> is missing in <a>missing-oaidc</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/no-dc-creator');
        $this->assertEquals(
            '<code>dc:creator</code> is missing in <a>record-1</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/no-dc-creator-orcid');
        $this->assertEquals(
            'No <code>dc:creator</code> with valid ORCID iD in <a>record-1</a>',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
