export function formatDuration (seconds) {
  const d = Math.floor(seconds / 86_400)
  const h = Math.floor((seconds % 86_400) / 3600)
  const min = Math.floor(((seconds % 86_400) % 3600) / 60)
  const s = Math.round(((seconds % 86_400) % 3600) % 60)

  return [
    d ? `${d}&#8239;d` : '',
    d || h ? `${h}&#8239;h` : '',
    d || h || min ? `${min}&#8239;min` : '',
    !d && !h && min < 10 ? `${s}&#8239;s` : '',
  ].join(' ')
}

export function formatTimestamp (timestamp) {
  const timezoneOffset = new Date().getTimezoneOffset() * 60_000
  return new Date(timestamp * 1000 - timezoneOffset).toISOString().slice(0, 19).replace('T', ' ')
}
