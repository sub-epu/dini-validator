<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_10_1Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListSets/bad');
        $this->assertEquals(
            'Sets for publication status are missing in <a>ListSets</a>.',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListSets/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
