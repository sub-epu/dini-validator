# Setup

## Local Development

You need to have installed (versions are minimum recommendations):

- [PHP](https://www.php.net/) 8.1
- [Composer](https://getcomposer.org/) 2.5
- [PCOV](https://github.com/krakjoe/pcov/blob/develop/INSTALL.md) 1.0 (required for code coverage reports)
- [Node.js](https://nodejs.org/) 18.17
- [Docker](https://www.docker.com/) 20.10 with [Compose](https://docs.docker.com/compose/) 1.29

Install frontend dependencies:

```sh
npm ci
```

Install API (backend) dependencies:

```sh
composer install
```

Start the API server and compile the frontend with hot-reload enabled:

```sh
docker-compose up -d
npm run dev
```

Then open <http://localhost:5173/> in your browser.

See `scripts` in [package.json](../package.json) for all available npm scripts.

## Coding Style and Linting

Backend code follows the [Laravel Coding Style](https://github.com/laravel/pint/blob/main/resources/presets/laravel.php) with some tightening (see [pint.json](pint.json)).

Frontend code follows the [JavaScript Standard Style](https://standardjs.com/) with a few extensions (see [.eslintrc.cjs](../.eslintrc.cjs) and [.stylelintrc.js](../.stylelintrc.js)) and adheres to the official [Vue.js Style Guide](https://vuejs.org/style-guide/).

Linting ensures a consistent coding style and reveals common bugs and bad practices during development. The API’s PHP code is linted with [Pint](https://github.com/laravel/pint) and [PHPStan](https://phpstan.org/) while the frontend’s Vue.js and SCSS code is linted with [ESLint](https://eslint.org/) and [Stylelint](https://stylelint.io/) respectively.

To lint the whole codebase:

```sh
npm run lint
```

To lint and fix all issues that can be fixed automatically:

```sh
npm run lint-fix
```

This should be run regularly, at the very least before each commit.

## Unit Tests

There are unit tests for every rule file in `api/rules`, covering every possible outcome. When a rule’s code is changed, the associated tests must be updated as well. When new rules are added, remember to add unit tests as well. It is highly recommended that every rule file has its own test file, which should be named like the class with `Test` appended, e.g. for `M_9_1.php`, the test file is `M_9_1Test.php`.

Unit tests use [PHPUnit](https://phpunit.de/) with [PCOV](https://github.com/krakjoe/pcov) for code coverage analysis. To run all tests:

```sh
npm run test
```

The output shows the test results for every rule file and includes a code coverage report. There should be no errors, warnings or issues, and the code coverage report should show 100&nbsp;% for classes, methods, and lines. In short, everything should be green. If not, extend the tests.

## Deployment

Follow these steps to deploy the application on a production system.

1. Ensure there are no obvious issues by running `npm run lint` and `npm run test`.

2. Compile the frontend:

    ```sh
    npm run build
    ```

3. Configure your web server:

    The server must have [PHP 8.1](https://www.php.net/releases/8.1/en.php) or later with the [Semaphore extension](https://www.php.net/manual/en/book.sem.php) installed. On [Apache](https://httpd.apache.org/), the modules `headers` and `rewrite` must be enabled.

    The app is expected to run under its own domain like `https://validator.example.org/` with `dist` as the document root.

    Configure the server so that

    - requests to `api/` are rewritten to `dist/api.php` and
    - all other requests are rewritten to `dist/index.html`, unless a file is requested.

    See [public/.htaccess](../public/.htaccess) for an Apache configuration example.

3. Copy these directories to your server:

    - `api`
    - `dist` (document root)
    - `vendor`

4. 🍰
