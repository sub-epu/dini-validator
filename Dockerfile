FROM php:8.2-apache

# Install required PHP extensions
RUN docker-php-ext-install sysvmsg
RUN docker-php-ext-install sysvsem
RUN docker-php-ext-install sysvshm
RUN docker-php-ext-configure sysvmsg --enable-sysvmsg
RUN docker-php-ext-configure sysvsem --enable-sysvsem
RUN docker-php-ext-configure sysvshm --enable-sysvshm

# Configure Apache
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' \
	/etc/apache2/apache2.conf
RUN a2enmod headers rewrite
