<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_9_3_4Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListRecords/good', ['unknownRecordId' => '']);
        $this->assertEquals(
            (object) [
                'verb' => 'GetRecord&identifier=',
                'text' => 'Invalid response to <a>$1</a>',
                'values' => ['GetRecord'],
            ],
            $result->issues[0],
        );

        $result = runRule('ListRecords/good', ['unknownRecordId' => 'invalid']);
        $this->assertEquals('GetRecord&identifier=invalid', $result->issues[0]->verb);
        $this->assertEquals('Schema validation errors in <a>$1</a>:<br>$2', $result->issues[0]->text);
        $this->assertEquals('GetRecord', $result->issues[0]->values[0]);
        $this->assertIsString($result->issues[0]->values[1]);

        $result = runRule('ListRecords/good');
        $this->assertEquals(0, $result->issuesCount);
    }
}
