<?php

declare(strict_types=1);

namespace Dini\Validator;

class M_9_6 extends Rule
{
    public string $oaiVerb = 'Identify';

    private $languageDetector;

    public function setup(): void
    {
        $this->languageDetector = new \LanguageDetection\Language();
    }

    public function check($xml, $isLastBatch): void
    {
        if (! filter_var($xml->Identify->adminEmail, FILTER_VALIDATE_EMAIL)) {
            $this->addIssue(
                'Identify',
                '<code>adminEmail</code> in <a>Identify</a> does not contain a valid email address.',
            );

            return;
        }

        if (! $xml->Identify->description) {
            $this->addIssue(
                'Identify',
                '<code>$1</code> is missing in <a>$2</a>',
                'description',
                'Identify',
            );

            return;
        }

        foreach ($xml->Identify->description as $description) {
            // NOTE: description can contain XML, see
            // https://www.openarchives.org/OAI/openarchivesprotocol.html#Identify
            $text = @dom_import_simplexml($description)->textContent;

            if ($text) {
                $language = (string) $this->languageDetector->detect($text);

                if ($language === 'en') {
                    return;
                }
            }
        }

        $this->addIssue(
            'Identify',
            '<a>Identify</a> does not seem to have a <code>description</code> in English.',
        );
    }
}
