<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_10_4 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    public function check($xml, $isLastBatch): void
    {
        if (empty($xml->ListRecords->resumptionToken)) {
            $this->finish('Not relevant due to small repository size');

            return;
        }

        if (empty($xml->ListRecords->resumptionToken['completeListSize'])) {
            $this->addFatalIssue(
                'ListRecords',
                '<code>$1</code> without <code>$2</code> in <a>$3</a>',
                'resumptionToken',
                'completeListSize',
                'ListRecords',
            );

            return;
        }

        $this->finish();
    }
}
