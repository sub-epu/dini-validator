<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_9_3 extends Rule
{
    public string $oaiVerb = 'ListRecords';

    public function checkRecord($record): void
    {
        if ($record->about && ! $record->about->provenance) {
            $this->addIssue(
                "GetRecord&identifier={$record->header->identifier}",
                '<code>$1</code> is set, but <code>$2</code> is missing in <a>$3</a>',
                'about',
                'provenance',
                (string) $record->header->identifier,
            );
        }
    }
}
