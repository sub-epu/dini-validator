<?php

declare(strict_types=1);

namespace Dini\Validator;

class E_10_2Test extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $result = runRule('ListRecords/small-repo');
        $this->assertEquals(0, $result->issuesCount);

        $result = runRule('ListRecords/batchsize-too-small');
        $this->assertEquals(
            'The batch size of <a>ListRecords</a> is 2.',
            getIssueText($result->issues[0]),
        );

        $result = runRule('ListRecords/fake-100');
        $this->assertEquals(0, $result->issuesCount);
    }
}
